<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MbcKeranjang extends Model
{
    protected $table = "mbc_keranjang";
    protected $fiilable = [
        "id",
        "id_distributor",
        "kode_produk",
        "qty",
        "harga",
        "jumlah",
        "created_at",
        "updated_at",
    ];
}
