<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Session;

use Closure;

class SesiTahunAkd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('thn_akd')) {
            return $next($request);
        }

        return redirect('/pilih-sesi');
    }
}
