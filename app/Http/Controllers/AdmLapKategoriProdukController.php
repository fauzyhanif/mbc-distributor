<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AdmLapKategoriProdukController extends Controller
{
    public function index(Request $resquest)
    {
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');

        if (request()->isMethod('post')) {
            $firstDay = explode(" - ", $resquest->get('tgl_transaksi'))[0];
            $lastDay = explode(" - ", $resquest->get('tgl_transaksi'))[1];
        }

        $datas = DB::table('mbc_kategori_produk')
            ->leftJoin('mbc_produk', 'mbc_kategori_produk.id', '=', 'mbc_produk.kategori_produk')
            ->leftJoin('mbc_transaksi_dtl', 'mbc_produk.kode', '=', 'mbc_transaksi_dtl.kode_produk')
            ->leftJoin('mbc_transaksi', 'mbc_transaksi_dtl.no_invoice', '=', 'mbc_transaksi.no_invoice')
            ->selectRaw('
                mbc_transaksi_dtl.kode_produk,
                mbc_kategori_produk.nama,
                sum(mbc_produk.stok) as stok,
                sum(mbc_transaksi_dtl.qty) as terjual,
                sum(mbc_transaksi_dtl.jumlah -
                    mbc_produk.harga_modal *
                    mbc_transaksi_dtl.qty) AS keuntungan
            ')
            ->whereBetween('mbc_transaksi.created_at', [$firstDay, $lastDay])
            ->orderBy('terjual', 'DESC')
            ->groupBy('mbc_transaksi_dtl.kode_produk')
            ->get();


        return view('admin.mbc.laporan.kategoriProduk.index', \compact('datas', 'firstDay', 'lastDay'));
    }

    public function detail($id, $firstDay, $lastDay)
    {
        $category = DB::table('mbc_kategori_produk')->where('id', '=', $id)->first();
        $datas = DB::table('mbc_transaksi_dtl')
            ->leftJoin('mbc_transaksi', 'mbc_transaksi.no_invoice', '=', 'mbc_transaksi_dtl.no_invoice')
            ->leftJoin('mbc_produk', 'mbc_transaksi_dtl.kode_produk', '=', 'mbc_produk.kode')
            ->select('mbc_produk.nama', DB::raw('SUM(mbc_produk.stok) as stok,
                    SUM(mbc_transaksi_dtl.qty) as terjual, ifnull(sum(mbc_transaksi_dtl.jumlah),0) -
                    mbc_produk.harga_modal *
                    ifnull(sum(mbc_transaksi_dtl.qty),0) AS keuntungan'))
            ->where('mbc_produk.kategori_produk', '=', $id)
            ->whereBetween('mbc_transaksi.tgl_transaksi', [$firstDay, $lastDay])
            ->groupBy('mbc_produk.kode', 'mbc_produk.nama', 'mbc_produk.harga_modal')
            ->get();

            return view('admin.mbc.laporan.kategoriProduk.detail', \compact('category', 'datas', 'firstDay', 'lastDay'));
    }
}
