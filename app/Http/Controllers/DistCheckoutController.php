<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;

use App\MbcGnrtInvoice;
use App\MbcKeranjang;
use App\MbcTransaksi;
use App\MbcTransaksiDtl;

class DistCheckoutController extends Controller
{
    public function index()
    {
        $idDistributor = Session::get('id_distributor');
        $myCart = DB::table('mbc_keranjang as a')
                ->leftJoin('mbc_produk as b', 'a.kode_produk', '=', 'b.kode')
                ->select('a.id', 'a.kode_produk', 'a.harga', 'a.qty', 'a.jumlah', 'b.nama', 'b.berat')
                ->where('id_distributor', '=', $idDistributor)
                ->get();

        $myProfile = DB::table('mbc_distributor')
                ->leftJoin('tb_ro_provinces', 'mbc_distributor.id_provinsi', '=',  'tb_ro_provinces.province_id')
                ->leftJoin('tb_ro_cities', 'mbc_distributor.id_kota', '=',  'tb_ro_cities.city_id')
                ->leftJoin('tb_ro_subdistricts', 'mbc_distributor.id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
                ->select('mbc_distributor.nama', 'mbc_distributor.no_hp', 'mbc_distributor.id_kecamatan',
                        'mbc_distributor.alamat', 'tb_ro_provinces.province_name',
                        'tb_ro_cities.city_name', 'tb_ro_subdistricts.subdistrict_name')
                ->where('mbc_distributor.id', '=', $idDistributor)
                ->first();

        $costs = $this->getCost($idDistributor);

        $provinces = DB::table('tb_ro_provinces')
            ->orderBy('province_name', 'ASC')
            ->get();

        return view('distributor.checkout.index', \compact('myCart', 'myProfile', 'costs', 'provinces'));
    }

    public function getCost($idDistributor)
    {
        // get origin unit
        $unit = DB::table('sys_ref_setting')->where('id_unit', '=', 1)->first();

        // get destination distributor
        $distributor = DB::table('mbc_distributor')->where('id', '=', $idDistributor)->first();

        // get weight goods
        $goods = DB::table('mbc_keranjang as a')
            ->leftJoin('mbc_produk as b', 'a.kode_produk', '=', 'b.kode')
            ->select('a.qty', 'b.berat')
            ->where('id_distributor', '=', $idDistributor)
            ->get();

        $weight = 0;

        foreach ($goods as $key => $value) {
            $weight = $weight + ($value->qty * $value->berat);
        }

        // get costs
        $response = Curl::to('https://pro.rajaongkir.com/api/cost')
            ->withHeader("key: 4ef712a6359604345f2d87ae27ce9396")
            ->withData([
                "origin" => "$unit->id_kecamatan",
                "originType" => "subdistrict",
                "destination" => "$distributor->id_kecamatan",
                "destinationType" => "subdistrict",
                "weight" => "$weight",
                "courier" => "jne:jnt"
            ])
            ->asJson()
            ->post();

        return $response->rajaongkir->results;
    }

    public function getCostDropshipper(Request $request)
    {
        $idKecamatan = $request->get("id_kecamatan");
        $weight = $request->get("weight");

        // get origin unit
        $unit = DB::table('sys_ref_setting')->where('id_unit', '=', 1)->first();

        // get costs
        $response = Curl::to('https://pro.rajaongkir.com/api/cost')
            ->withHeader("key: 4ef712a6359604345f2d87ae27ce9396")
            ->withData([
                "origin" => "$unit->id_kecamatan",
                "originType" => "subdistrict",
                "destination" => "$idKecamatan",
                "destinationType" => "subdistrict",
                "weight" => "$weight",
                "courier" => "jne:jnt"
            ])
            ->asJson()
            ->post();

        $costs = $response->rajaongkir->results;
        return view('distributor.checkout.costs', \compact('costs'));
    }

    public function createOrder(Request $request)
    {
        $idDistributor = Session::get('id_distributor');
        $requests = $request->all();

        // generate invoice
        $requests['no_invoice'] = $this->getInvoiceId();

        // fetch logistik = name, service, day, cost
        $requests['logistik_name'] = explode(" * ", $request->get('logistik'))[0];
        $requests['logistik_service'] = explode(" * ", $request->get('logistik'))[1];
        $requests['logistik_day'] = explode(" * ", $request->get('logistik'))[2];
        $requests['ttl_ongkir'] = explode(" * ", $request->get('logistik'))[3];
        $requests['request_jtr'] = "N";
        $requests['konfirmasi_jtr'] = "N";

        // get amount from cart
        $amounts = $this->getAmount($idDistributor);
        $requests['ttl_qty'] = $amounts["ttl_qty"];
        $requests['ttl_belanja'] = $amounts["ttl_belanja"];
        $requests['ttl_berat'] = $amounts["ttl_berat"];
        $requests['ttl_transaksi'] = $amounts["ttl_belanja"] + $requests['ttl_ongkir'];

        // get profile
        $profile = $this->getProfile($idDistributor);
        $requests['nama'] = $profile->nama;
        $requests['no_hp'] = $profile->no_hp;
        $requests['alamat'] = $profile->alamat;
        $requests['id_kecamatan'] = $profile->id_kecamatan;
        $requests['id_kota'] = $profile->id_kota;
        $requests['id_provinsi'] = $profile->id_provinsi;

        // save transaction
        $this->saveTransaction($requests, $idDistributor);

        // get goods from cart
        $this->saveTransactionDtl($requests['no_invoice'], $idDistributor);

        // empty cart
        $this->emptyCart($idDistributor);

        // redirect to success order
        return json_encode($requests['no_invoice']);
    }

    public function requestJtr(Request $request)
    {
        $idDistributor = Session::get('id_distributor');
        $requests = $request->all();

        // generate invoice
        $requests['no_invoice'] = $this->getInvoiceId();

        // fetch logistik = name, service, day, cost
        $requests['logistik_name'] = "";
        $requests['logistik_service'] = "";
        $requests['logistik_day'] = "";
        $requests['ttl_ongkir'] = "0";
        $requests['request_jtr'] = "Y";
        $requests['konfirmasi_jtr'] = "N";

        // get amount from cart
        $amounts = $this->getAmount($idDistributor);
        $requests['ttl_qty'] = $amounts["ttl_qty"];
        $requests['ttl_belanja'] = $amounts["ttl_belanja"];
        $requests['ttl_berat'] = $amounts["ttl_berat"];
        $requests['ttl_transaksi'] = $amounts["ttl_belanja"] + $requests['ttl_ongkir'];

        // get profile
        $profile = $this->getProfile($idDistributor);
        $requests['nama'] = $profile->nama;
        $requests['no_hp'] = $profile->no_hp;
        $requests['alamat'] = $profile->alamat;
        $requests['id_kecamatan'] = $profile->id_kecamatan;
        $requests['id_kota'] = $profile->id_kota;
        $requests['id_provinsi'] = $profile->id_provinsi;

        // save transaction
        $this->saveTransaction($requests, $idDistributor);

        // get goods from cart
        $this->saveTransactionDtl($requests['no_invoice'], $idDistributor);

        // empty cart
        $this->emptyCart($idDistributor);

        // redirect to success order
        return json_encode($requests['no_invoice']);
    }

    public function getInvoiceId()
    {
        $thisMonth = date('Y-m');
        $invoiceId = 'INV';

        $lastInoviceId = DB::table('mbc_gnrt_invoice')->where('bulan', '=', "$thisMonth")->first();

        // check exist or not
        if ( $lastInoviceId === null ) {
            $orderId = "0001";

            $new = new MbcGnrtInvoice();
            $new->bulan = "$thisMonth";
            $new->urut = "0002";
            $new->save();
        } else {
            $orderId = sprintf('%04d', $lastInoviceId->urut);
            $update = DB::table('mbc_gnrt_invoice')
                ->where('bulan', '=', "$thisMonth")
                ->update(["urut" => $orderId + 1]);
        }

        $invoiceId .= str_replace("-", "", $thisMonth) . $orderId;
        return $invoiceId;
    }

    public function getAmount($idDistributor)
    {
        $amount = DB::table('mbc_keranjang as a')
            ->leftJoin('mbc_produk as b', 'a.kode_produk', '=', 'b.kode')
            ->where('id_distributor', '=', $idDistributor)
            ->selectRaw('sum(a.qty * b.berat) as berat, sum(a.qty) as qty, sum(a.jumlah) as jumlah')
            ->first();

        $result = [
            "ttl_berat" => $amount->berat,
            "ttl_qty" => $amount->qty,
            "ttl_belanja" => $amount->jumlah
        ];

        return $result;
    }

    public function getProfile($idDistributor)
    {
        $profile = DB::table('mbc_distributor')->where('id', '=', $idDistributor)->first();

        return $profile;
    }

    public function saveTransaction($requests, $idDistributor)
    {
        date_default_timezone_set('Asia/Jakarta');

        $new = new MbcTransaksi();
        $new->tgl_transaksi = date('Y-m-d H:i:s');
        $new->no_invoice = $requests['no_invoice'];
        $new->id_distributor = $idDistributor;
        $new->no_invoice = $requests['no_invoice'];
        $new->nama = $requests['nama'];
        $new->no_hp = $requests['no_hp'];
        $new->alamat = $requests['alamat'];
        $new->id_kecamatan = $requests['id_kecamatan'];
        $new->id_kota = $requests['id_kota'];
        $new->id_provinsi = $requests['id_provinsi'];
        $new->is_dropshipper = $requests['is_dropshipper'];
        $new->dropship_nama = $requests['dropship_nama'];
        $new->dropship_no_hp = $requests['dropship_no_hp'];
        $new->dropship_alamat = $requests['dropship_alamat'];
        $new->dropship_id_kecamatan = $requests['id_kecamatan'];
        $new->dropship_id_kota = $requests['dropship_id_kota'];
        $new->dropship_id_provinsi = $requests['dropship_id_provinsi'];
        $new->logistik_name = $requests['logistik_name'];
        $new->logistik_service = $requests['logistik_service'];
        $new->logistik_day = $requests['logistik_day'];
        $new->request_jtr = $requests['request_jtr'];
        $new->konfirmasi_jtr = $requests['konfirmasi_jtr'];
        $new->logistik_day = $requests['logistik_day'];
        $new->ttl_ongkir = $requests['ttl_ongkir'];
        $new->ttl_qty = $requests['ttl_qty'];
        $new->ttl_berat = $requests['ttl_berat'];
        $new->ttl_belanja = $requests['ttl_belanja'];
        $new->ttl_transaksi = $requests['ttl_transaksi'];
        $new->catatan = $requests['catatan'];

        $new->save();
    }

    public function saveTransactionDtl($invoiceId, $idDistributor)
    {
        $goods = DB::table('mbc_keranjang')->where('id_distributor', '=', $idDistributor)->get();

        foreach ($goods as $key => $value) {
            $new = new MbcTransaksiDtl();
            $new->no_invoice = $invoiceId;
            $new->kode_produk = $value->kode_produk;
            $new->qty = $value->qty;
            $new->harga = $value->harga;
            $new->jumlah = $value->jumlah;
            $new->save();
        }
    }

    public function emptyCart($idDistributor)
    {
        $cart = MbcKeranjang::where('id_distributor', '=', $idDistributor)->delete();
    }

    public function pembayaran($invoiceId)
    {
        $invoice = DB::table('mbc_transaksi')
            ->where('no_invoice', '=', "$invoiceId")
            ->selectRaw('ttl_transaksi, no_invoice')
            ->first();

        return view('distributor.checkout.pembayaran', compact('invoice'));
    }

    public function waitingRequestJtr($invoiceId)
    {
        return view('distributor.checkout.waitingRequestJtr', \compact('invoiceId'));
    }
}

/*
Daftar kurir
pos, tiki, jne, pcp, esl, rpx, pandu, wahana, jnt, pahala, cahaya, sap, jet, indah, dse, slis, first, ncs, dan star
*/
