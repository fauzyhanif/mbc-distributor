<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AdmLapProdukController extends Controller
{
    public function index(Request $resquest)
    {
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');

        if (request()->isMethod('post')) {
            $firstDay = explode(" - ", $resquest->get('tgl_transaksi'))[0];
            $lastDay = explode(" - ", $resquest->get('tgl_transaksi'))[1];
        }

        $products = DB::table('mbc_produk')->get();
        $arrProducts =[];
        foreach ($products as $key => $value) {
            $arrProducts[$value->kode] = $value->stok;
        }

        $datas = DB::table('mbc_transaksi_dtl')
            ->leftJoin('mbc_transaksi', 'mbc_transaksi.no_invoice', '=', 'mbc_transaksi_dtl.no_invoice')
            ->leftJoin('mbc_produk', 'mbc_transaksi_dtl.kode_produk', '=', 'mbc_produk.kode')
            ->select('mbc_produk.kode', 'mbc_produk.nama', DB::raw('SUM(mbc_produk.stok) as stok,
                    SUM(mbc_transaksi_dtl.qty) as terjual, ifnull(sum(mbc_transaksi_dtl.jumlah),0) -
                    mbc_produk.harga_modal *
                    ifnull(sum(mbc_transaksi_dtl.qty),0) AS keuntungan'))
            ->whereBetween('mbc_transaksi.tgl_transaksi', [$firstDay, $lastDay])
            ->groupBy('mbc_produk.kode', 'mbc_produk.nama', 'mbc_produk.harga_modal')
            ->orderBy('terjual', 'DESC')
            ->get();

        return view('admin.mbc.laporan.produk.index', \compact('datas', 'firstDay', 'lastDay', 'arrProducts'));
    }
}
