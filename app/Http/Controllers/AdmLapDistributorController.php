<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AdmLapDistributorController extends Controller
{
    public function index(Request $resquest)
    {
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');

        if (request()->isMethod('post')) {
            $firstDay = explode(" - ", $resquest->get('tgl_transaksi'))[0];
            $lastDay = explode(" - ", $resquest->get('tgl_transaksi'))[1];
        }

        $datas = DB::table('mbc_transaksi')
                ->leftJoin('mbc_distributor', 'mbc_transaksi.id_distributor', '=', 'mbc_distributor.id')
                ->leftJoin('tb_ro_cities', 'mbc_distributor.id_kota', '=',  'tb_ro_cities.city_id')
                ->select('mbc_distributor.nama', 'tb_ro_cities.city_name',
                        DB::raw('SUM(mbc_transaksi.ttl_belanja) as jml_omset,
                        SUM(mbc_transaksi.ttl_qty) as jml_qty
                        '))
                ->where('mbc_transaksi.stts_umum', '!=', '0')
                ->whereBetween('mbc_transaksi.tgl_transaksi', [$firstDay, $lastDay])
                ->orderBy('jml_omset', 'DESC')
                ->groupBy('mbc_transaksi.id_distributor', 'mbc_distributor.nama', 'tb_ro_cities.city_name')
                ->get();

        return view('admin.mbc.laporan.distributor.index', \compact('datas', 'firstDay', 'lastDay'));
    }
}
