<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class DistPesananController extends Controller
{
    public function index($sttsUmum = 0)
    {
        $idDistributor = Session::get('id_distributor');

        $listOrder = DB::table('mbc_transaksi')
            ->leftJoin('mbc_stts_transaksi', 'mbc_transaksi.stts_umum', '=', 'mbc_stts_transaksi.kode')
            ->select('mbc_transaksi.no_invoice', 'mbc_transaksi.is_dropshipper', 'mbc_transaksi.tgl_transaksi',
                    'mbc_transaksi.ttl_transaksi', 'mbc_stts_transaksi.nama as status', 'mbc_transaksi.request_jtr', 'mbc_transaksi.konfirmasi_jtr')
            ->where('mbc_transaksi.id_distributor', '=', $idDistributor)
            ->where('mbc_transaksi.stts_umum', '=', "$sttsUmum")
            ->orderBy('mbc_transaksi.tgl_transaksi', 'DESC')
            ->paginate(10);

        $status = DB::table('mbc_stts_transaksi')->get();

        return view('distributor.pesanan.index', \compact('listOrder', 'status', 'sttsUmum'));
    }

    public function detail($invoiceId)
    {
        $myOrder = DB::table('mbc_transaksi_dtl as a')
                ->leftJoin('mbc_produk as b', 'a.kode_produk', '=', 'b.kode')
                ->select('a.id', 'a.kode_produk', 'a.harga', 'a.qty', 'a.jumlah', 'b.nama', 'b.berat')
                ->where('no_invoice', '=', $invoiceId)
                ->get();

        $transaction = DB::table('mbc_transaksi')
                ->leftJoin('tb_ro_provinces', 'mbc_transaksi.id_provinsi', '=',  'tb_ro_provinces.province_id')
                ->leftJoin('tb_ro_cities', 'mbc_transaksi.id_kota', '=',  'tb_ro_cities.city_id')
                ->leftJoin('tb_ro_subdistricts', 'mbc_transaksi.id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
                ->leftJoin('mbc_stts_transaksi', 'mbc_transaksi.stts_umum', '=', 'mbc_stts_transaksi.kode')
                ->select('mbc_transaksi.*',
                        'mbc_stts_transaksi.nama as nm_stts_umum',
                        'tb_ro_provinces.province_name',
                        'tb_ro_cities.city_name', 'tb_ro_subdistricts.subdistrict_name')
                ->where('no_invoice', '=', $invoiceId)
                ->first();

        $dropship = DB::table('mbc_transaksi')
                ->leftJoin('tb_ro_provinces', 'mbc_transaksi.dropship_id_provinsi', '=',  'tb_ro_provinces.province_id')
                ->leftJoin('tb_ro_cities', 'mbc_transaksi.dropship_id_kota', '=',  'tb_ro_cities.city_id')
                ->leftJoin('tb_ro_subdistricts', 'mbc_transaksi.dropship_id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
                ->select(
                        'mbc_transaksi.dropship_nama', 'mbc_transaksi.dropship_no_hp', 'mbc_transaksi.dropship_alamat',
                        'tb_ro_provinces.province_name',
                        'tb_ro_cities.city_name', 'tb_ro_subdistricts.subdistrict_name')
                ->where('no_invoice', '=', $invoiceId)
                ->first();

        return view('distributor.pesanan.detail', \compact('myOrder', 'transaction', 'dropship'));
    }

    public function konfirmasiSampai(Request $request)
    {
        $invoiceId = $request->get('no_invoice');
        $sttsUmum = $request->get('stts_umum');

        $update = DB::table('mbc_transaksi')
                ->where('no_invoice', '=', "$invoiceId")
                ->update(["stts_umum" => $sttsUmum]);

        return redirect('/dist/pesanan/detail/' . $invoiceId);
    }
}
