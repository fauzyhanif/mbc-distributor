<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

use App\SysRefUser;
use App\SysRefUsergroup;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.indexNew');
    }

    public function doAuth(Request $request)
    {
        $request->validate([
            "username" => "required",
            "password" => "required"
        ]);

        $data_exist = SysRefUser::where('username', '=', $request->get('username'))->first();
        if ($data_exist === null) {
            return redirect('/auth')->withErrors(['Username salah.', '']);
        } else {
            if (!Hash::check($request->get('password'), $data_exist->password)) {
                return redirect('/auth')->withErrors(['Username atau password salah.', '']);
            } else {
                // set session
                Session::put('nama', $data_exist->nama);
                Session::put('username', $data_exist->username);
                Session::put('usergroup', $data_exist->id_usergroup);
                Session::put('jenjang', $data_exist->id_jenjang);

                // cek jika distributor atau bukan
                if ($data_exist->id_usergroup == ',3,') {
                    // jika distributor maka redirect profil santri
                    Session::put('id_distributor', $data_exist->id_distributor);
                    Session::put('usergroup_aktif', str_replace(",", "", $data_exist->id_usergroup));
                    return redirect('/dist/dashboard');
                } else {
                    // jika admin maka redirect pilih usergroup
                    return redirect('/auth/pilih-usergroup');
                }
            }
        }
    }

    public function pilihUsergroup(Request $request)
    {
        $sessionUsergroup = Session::get('usergroup');
        $dtUsergroup = SysRefUsergroup::where('id_usergroup', '!=', '3')->get();
        return view('auth.pilihUsergroup', compact('sessionUsergroup', 'dtUsergroup'));
    }

    public function setUsergroup(Request $request, $id)
    {
        Session::put('usergroup_aktif', $id);

        $nmUsergroup = SysRefUsergroup::where('id_usergroup', '=', $id)->first();
        Session::put('nama_usergroup_aktif', $nmUsergroup->nama);
        return redirect('/dashboard');
    }

    public function logout()
    {
        Session::flush();
        return redirect('/auth');
    }

    public function accessDenied()
    {
        return view('auth/accessDenied');
    }

    public function CheckSessionWithJs()
    {
        $session = Session::get('username');
        return json_encode($session);
    }

    public function loginUlang()
    {
        return view('distributor.error.loginUlang');
    }
}
