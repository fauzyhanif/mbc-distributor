<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;

use App\MbcKategoriProduk;

class KategoriProdukController extends Controller
{
    public function index()
    {
        $datas = DB::table('mbc_kategori_produk')
            ->orderBy('nama', 'ASC')
            ->get();

        return view('admin.mbc.kategoriProduk.index', compact('datas'));
    }

    public function formAdd()
    {
        return view('admin.mbc.kategoriProduk.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'nama'=>'required'
        ]);

        $newData = new MbcKategoriProduk();
        $newData->nama = $request->get('nama');
        $newData->save();

        return redirect('/admin/kategori-produk')->with('success', 'Distributor berhasil ditambahkan');
    }

    public function formEdit($id)
    {
        $data = MbcKategoriProduk::find($id);

        return view('admin.mbc.kategoriProduk.formEdit', compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required'
        ]);

        $oldData = MbcKategoriProduk::find($id);
        $oldData->nama = $request->get('nama');

        $oldData->update();

        return redirect('/admin/kategori-produk')->with('success', 'Distributor berhasil diperbaharui');
    }

    public function aktifasi(Request $request, $id)
    {
        $status = $request->get('is_aktif');
        $aktifasi = DB::table('mbc_kategori_produk')
            ->where('id', '=', $id)
            ->update(["is_aktif" => "$status"]);

        $modofiedData = DB::table('mbc_kategori_produk')
                    ->select('nama')
                    ->where('id', '=', $id)
                    ->first();

        $def = "";
        if ($status == "Y") {
            $def = "diaktifkan";
        } else {
            $def = "dinonaktifkan";
        }

        return redirect('/admin/kategori-produk')->with('success', "Kategori produk $modofiedData->nama berhasil $def.");
    }

    public function delete(Request $request, $id)
    {
        $data = MbcKategoriProduk::find($id);
        $data->delete();

        return redirect('/admin/kategori-produk')->with('success', 'Distributor berhasil dihapus');
    }
}
