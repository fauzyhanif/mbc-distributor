<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SessionController extends Controller
{
    public function pilihSesiTahunAkd()
    {
        $datas = DB::table('psb_ref_sesi')
                    ->orderBy('thn_akd', 'desc')
                    ->get();
        return view('session.pilihSesi', \compact('datas'));
    }

    public function setSesiTahunAkd($thnAkd, $gelombang)
    {
        Session::put('thn_akd', $thnAkd);
        Session::put('gelombang', $gelombang);

        return redirect('/data-calon-santri');
    }
}
