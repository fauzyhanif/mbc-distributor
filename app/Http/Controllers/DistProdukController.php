<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\MbcKategoriProduk;
use App\MbcFotoProduk;
use App\MbcProduk;

class DistProdukController extends Controller
{
    public function index($kategori = '*')
    {
        $categories = MbcKategoriProduk::all();
        $products = MbcProduk::where("is_aktif", "Y")->paginate(12);

        if ($kategori != '*') {
            $products = MbcProduk::where('kategori_produk', '=', $kategori)
                        ->where("is_aktif", "Y")
                        ->paginate(12);
        }

        return view('distributor.produk.index', compact('kategori', 'categories', 'products'));
    }

    public function search(Request $request)
    {
        $key = $request->get('key');
        if (request()->isMethod('post')) {
            Session::put('key_search_produk', $key);
        }

        $keys = Session::get('key_search_produk');

        $count_products = DB::table('mbc_produk')
                            ->where("nama", "like", "%$keys%")
                            ->where("is_aktif", "Y")
                            ->get();

        $countResult = count($count_products);
        $products = DB::table('mbc_produk')
                    ->where("nama", "like", "%$keys%")
                    ->where("is_aktif", "Y")
                    ->paginate(12);

        return view('distributor.produk.search', compact('products', 'keys', 'countResult'));
    }

    public function detail($id)
    {
        $data = MbcProduk::find($id);
        $dtFoto = MbcFotoProduk::where("kode_produk", "=", $data->kode)->get();

        return view('distributor.produk.detail', compact('data', 'dtFoto'));
    }
}
