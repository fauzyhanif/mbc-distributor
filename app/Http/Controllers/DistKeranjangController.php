<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\MbcKeranjang;

class DistKeranjangController extends Controller
{
    public function index()
    {
        $idDistributor = Session::get('id_distributor');
        $countMyCart = 0;

        $myCart = DB::table('mbc_keranjang as a')
                ->leftJoin('mbc_produk as b', 'a.kode_produk', '=', 'b.kode')
                ->select('a.id', 'a.kode_produk', 'a.harga', 'a.qty', 'a.jumlah', 'b.nama')
                ->where('id_distributor', '=', $idDistributor)
                ->get();

        $countMyCart = count($myCart);
        return view('distributor.keranjang.index', compact('myCart', 'countMyCart'));
    }

    public function addToCart(Request $request)
    {
        $response = [];
        if (Session::has('id_distributor')) {
            $kodeProduk = $request->get('kode_produk');
            $idDistributor = Session::get('id_distributor');

            // get harga from master produk
            $dtProduk = DB::table('mbc_produk')
                        ->where('kode', '=', $kodeProduk)
                        ->first();

            // cek apakah produk ini sudah ada di keranjang
            $cek = DB::table('mbc_keranjang')
                    ->where('kode_produk', '=', $kodeProduk)
                    ->where('id_distributor', '=', $idDistributor)
                    ->first();

            if ($cek === null) {
                // save
                $keranjangBaru = new MbcKeranjang();
                $keranjangBaru->id_distributor = $idDistributor;
                $keranjangBaru->kode_produk = $kodeProduk;
                $keranjangBaru->qty = 1;
                $keranjangBaru->harga = $dtProduk->harga_jual;
                $keranjangBaru->jumlah = $dtProduk->harga_jual;
                $keranjangBaru->save();
            } else {
                $keranjangLama = MbcKeranjang::where('kode_produk', '=', $kodeProduk)->firstOrFail();
                $keranjangLama->qty = $cek->qty + 1;
                $keranjangLama->jumlah = $dtProduk->harga_jual * ($cek->qty + 1);
                $keranjangLama->update();
            }

            $response = [
                "status" => "success",
                "text" => "Ditambahkan ke keranjang"
            ];
        } else {
            $response = [
                "status" => "danger",
                "text" => "Anda harus login ulang."
            ];
        }

        return json_encode($response);
    }

    public function ReloadHeaderCountCart()
    {
        $idDistributor = Session::get('id_distributor');
        $countMyCart = 0;
        $myCart = DB::table('mbc_keranjang')->where('id_distributor', '=', $idDistributor)->get();

        $countMyCart = count($myCart);

        return $countMyCart;
    }

    public function ReloadHeaderListCart()
    {
        $idDistributor = Session::get('id_distributor');
        $countMyCart = 0;

        $myCart = DB::table('mbc_keranjang as a')
                ->leftJoin('mbc_produk as b', 'a.kode_produk', '=', 'b.kode')
                ->select('a.harga', 'b.nama', 'b.thumbnail')
                ->where('id_distributor', '=', $idDistributor)
                ->get();

        return $myCart;
    }

    public function updateDataItem(Request $request)
    {
        $id = $request->get('id');
        $qty = $request->get('qty');
        $jumlah = $request->get('jumlah');

        $cart = MbcKeranjang::find($id);
        $cart->qty = $qty;
        $cart->jumlah = $jumlah;
        $cart->update();
    }

    public function removeDataItem(Request $request)
    {
        $id = $request->get('id');

        $cart = MbcKeranjang::find($id);
        $cart->delete();
    }
}
