<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SysRefUsergroup;

class UsergroupController extends Controller
{
    public function index()
    {
        $datas = SysRefUsergroup::all();
        return view('admin.referensi.usergroup.index', compact('datas'));
    }
    public function formAdd(Request $request)
    {
        return view('admin.referensi.usergroup.add');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'id_usergroup'=>'required|integer|unique:sys_ref_usergroup',
            'nama'=>'required'
        ]);

        $new_data = new SysRefUsergroup();
        $new_data->id_usergroup = $request->get('id_usergroup');
        $new_data->nama = $request->get('nama');
        $new_data->save();

        return redirect('/usergroup')->with('success', 'Usergroup berhasil ditambahkan');
    }

    public function formEdit($id)
    {
        $data = SysRefUsergroup::where('id_usergroup', '=', $id)->first();
        return view('admin.referensi.usergroup.edit', compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'id_usergroup'=>'required|integer|unique:sys_ref_usergroup,id_usergroup,' . $id . ',id_usergroup',
            'nama'=>'required'
        ]);

        $data = SysRefUsergroup::find($id);
        $data->id_usergroup = $request->get('id_usergroup');
        $data->nama = $request->get('nama');
        $data->update();

        return redirect('/usergroup')->with('success', 'Usergroup berhasil diubah');
    }

    public function delete($id)
    {
        $data = SysRefUsergroup::find($id);
        $data->delete();

        return redirect('/usergroup')->with('success', 'Usergroup berhasil dihapus');
    }
}
