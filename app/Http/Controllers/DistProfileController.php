<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\MbcDistributor;

class DistProfileController extends Controller
{
    public function index()
    {
        $idDistributor = Session::get('id_distributor');

        $myProfile = DB::table('mbc_distributor')
            ->leftJoin('tb_ro_provinces', 'mbc_distributor.id_provinsi', '=',  'tb_ro_provinces.province_id')
            ->leftJoin('tb_ro_cities', 'mbc_distributor.id_kota', '=',  'tb_ro_cities.city_id')
            ->leftJoin('tb_ro_subdistricts', 'mbc_distributor.id_kecamatan', '=',  'tb_ro_subdistricts.subdistrict_id')
            ->select('mbc_distributor.*', 'tb_ro_provinces.province_name', 'tb_ro_cities.city_name', 'tb_ro_subdistricts.subdistrict_name')
            ->where('id', '=', $idDistributor)
            ->first();

        return view('distributor.profile.index', \compact('myProfile'));
    }

    public function formEdit()
    {
        $idDistributor = Session::get('id_distributor');
        $data = MbcDistributor::find($idDistributor);

        $dtProvinsi = DB::table('tb_ro_provinces')
            ->orderBy('province_name', 'ASC')
            ->get();

        $dtKota = DB::table('tb_ro_cities')
            ->where('province_id', '=', $data->id_provinsi)
            ->orderBy('city_name', 'ASC')
            ->get();

        $dtKecamatan = DB::table('tb_ro_subdistricts')
            ->where('city_id', '=', $data->id_kota)
            ->orderBy('subdistrict_name', 'ASC')
            ->get();

        return view('distributor.profile.edit', \compact('data', 'dtProvinsi', 'dtKota', 'dtKecamatan'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            'id_kecamatan'=>'required',
            'id_kota'=>'required',
            'id_provinsi'=>'required'
        ]);

        $oldData = MbcDistributor::find($id);
        $oldData->nama = $request->get('nama');
        $oldData->no_hp = $request->get('no_hp');
        $oldData->alamat = $request->get('alamat');
        $oldData->id_kecamatan = $request->get('id_kecamatan');
        $oldData->id_kota = $request->get('id_kota');
        $oldData->id_provinsi = $request->get('id_provinsi');
        $oldData->update();

        return redirect('/dist/profile/form-edit')->with('success', 'Data berhasil diperbaharui');
    }

    public function formUbahPassword()
    {
        return view('distributor.profile.formUbahPassword');
    }

    public function ubahPassword(Request $request)
    {
        $request->validate([
            'old_password'=>'required',
            'new_password'=>'required|min:6',
            'confirm_password'=>'required|min:6'
        ]);

        $oldPass = $request->get('old_password');
        $newPass = $request->get('new_password');
        $confirmPass = $request->get('confirm_password');

        $idDistributor = Session::get('id_distributor');
        $data = DB::table('sys_ref_user')
        ->where('id_distributor', '=', $idDistributor)
        ->first();

        if (Hash::check($oldPass, $data->password)) {
            if ($newPass == $confirmPass) {
                $update = DB::table('sys_ref_user')
                    ->where('id_distributor', '=', "$idDistributor")
                    ->update(["password" => Hash::make($newPass)]);
                    return redirect('/dist/form-ubah-password')->with('success','Password berhasil diperbaharui.');
            } else {
                return redirect('/dist/form-ubah-password')->with('error','Konfirmasi password salah.');
            }
        } else {
            return redirect('/dist/form-ubah-password')->with('error','Password lama salah.');
        }
    }
}
