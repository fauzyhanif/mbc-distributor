<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class DistDashboardController extends Controller
{
    public function index()
    {
        $year = date('Y');
        $month = date('m');
        $idDistributor = Session::get('id_distributor');

        $myProfile = DB::table('mbc_distributor')
                ->leftJoin('tb_ro_cities', 'mbc_distributor.id_kota', '=',  'tb_ro_cities.city_id')
                ->select('mbc_distributor.nama', 'tb_ro_cities.city_name')
                ->where('id', '=', $idDistributor)
                ->first();

        $total = DB::table('mbc_transaksi')
                ->where('id_distributor', '=', $idDistributor)
                ->where('stts_batal', '=', 0)
                ->where('stts_umum', '!=', 0)
                ->selectRaw('sum(ttl_transaksi) as ttl_omset, count(*) as ttl_transaksi')
                ->first();

        $omsetThisMonth = DB::table('mbc_transaksi')
                ->where('id_distributor', '=', $idDistributor)
                ->where('stts_batal', '=', 0)
                ->where('stts_umum', '!=', 0)
                ->whereYear('tgl_transaksi', '=', $year)
                ->whereMonth('tgl_transaksi', '=', $month)
                ->sum('ttl_transaksi');

        $listOrder = DB::table('mbc_transaksi')
            ->leftJoin('mbc_stts_transaksi', 'mbc_transaksi.stts_umum', '=', 'mbc_stts_transaksi.kode')
            ->select( 'mbc_transaksi.no_invoice', 'mbc_transaksi.is_dropshipper', 'mbc_transaksi.tgl_transaksi',
                        'mbc_transaksi.ttl_transaksi', 'mbc_stts_transaksi.nama as status', 'mbc_transaksi.request_jtr', 'mbc_transaksi.konfirmasi_jtr')
            ->where('mbc_transaksi.id_distributor', '=', $idDistributor)
            ->orderBy('mbc_transaksi.no_invoice', 'DESC')
            ->limit(5)
            ->get();
        
        return view('distributor.dashboard.index', \compact('myProfile', 'total', 'omsetThisMonth', 'listOrder'));
    }
}
