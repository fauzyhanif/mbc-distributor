<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        if (Session::has('username')) {
            if (Session::get('usergroup_aktif') == '1' || Session::get('usergroup_aktif') == '2' || Session::get('usergroup_aktif') == '99') {
                return view('dashboard.admin');
            } elseif (Session::get('usergroup_aktif') == '3') {
                return redirect('/dist/dashboard');
            }
        } else {
            return redirect('/auth');
        }
    }
}
