<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent;
use DataTables;

use App\SysRefUser;
use App\PsbRefJenjang;
use App\SysRefUsergroup;

class userController extends Controller
{
    public function index()
    {
        $datas = SysRefUser::all();
        $dt_usergroup = SysRefUsergroup::all();
        return view('admin.referensi.user.index', compact('datas', 'dt_usergroup'));
    }

    public function json(){
        $model = SysRefUser::query();
        return DataTables::eloquent($model)
                ->addColumn('jenis', function(SysRefUser $user) {
                    return  $user->id_usergroup == ',3,' ? 'Distributor' : 'Admin MBC';
                })
                ->addColumn('aksi', function(SysRefUser $user) {
                    $button = '<a href="user/reset-password/' . $user->id . '" class="btn btn-default btn-xs"><i class="fa fa-lock"></i> Reset Password</a> ';
                    $button .= '<a href="user/form-edit/' . $user->id . '" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Edit</a> ';
                    $button .= '<a href="user/delete/' . $user->id . '" class="btn btn-default btn-xs"><i class="fa fa-trash"></i> Hapus</a>';
                    return  $button;
                })
                ->rawColumns(['jenis', 'aksi'])
                ->toJson();
    }

    public function formAdd(Request $request)
    {
        $dt_usergroup = SysRefUsergroup::all();
        return view('admin.referensi.user.add', compact('dt_usergroup'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'nama'=>'required',
            'username'=>'required|unique:sys_ref_user',
            'password'=>'required|min:6',
            'id_usergroup'=>'required'
        ]);

        $new_data = new SysRefUser();
        $new_data->nama = $request->get('nama');
        $new_data->username = $request->get('username');
        $new_data->password = Hash::make($request->get('password'));
        $new_data->id_usergroup = "," . implode(",", $request->get('id_usergroup')) . ",";
        $new_data->save();

        return redirect('/user')->with('success', 'user berhasil ditambahkan');
    }

    public function formEdit($id)
    {
        $data           = SysRefUser::where('id', '=', $id)->first();
        $dt_usergroup   = SysRefUsergroup::all();
        return view('admin.referensi.user.edit', compact('data', 'dt_jenjang', 'dt_usergroup'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
            'username'=>'required|unique:sys_ref_user,username,' . $request->get('username') . ',username',
            'id_usergroup'=>'required',
        ]);

        $data = SysRefUser::find($id);
        $data->nama = $request->get('nama');
        $data->username = $request->get('username');
        $data->id_usergroup = "," . implode(",", $request->get('id_usergroup')) . ",";
        if ($request->get('password') != '') {
            $data->password = Hash::make($request->get('password'));
        }
        $data->update();

        return redirect('/user')->with('success', 'user berhasil diubah');
    }

    public function delete($id)
    {
        $data = SysRefUser::find($id);
        $data->delete();

        return redirect('/user')->with('success', 'user berhasil dihapus');
    }

    public function resetPassword($id)
    {
        $newPassword = Hash::make(123456);

        $data = SysRefUser::find($id);
        $data->password = $newPassword;
        $data->update();

        return redirect('/user')->with('success', 'Password user berhasil direset menjadi 123456.');
    }
}
