<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MbcProduk extends Model
{
    protected $table = "mbc_produk";
    protected $fiilable = [
        "id",
        "kode",
        "kategori_produk",
        "nama",
        "thumbnail",
        "berat",
        "stok",
        "harga_modal",
        "harga_jual",
        "is_aktif",
        "created_at",
        "updated_at",
    ];
}
