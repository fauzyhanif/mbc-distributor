<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MbcGnrtInvoice extends Model
{
    protected $table = "mbc_gnrt_invoice";
    protected $fiilable = [
        "bulan",
        "urut"
    ];
}
