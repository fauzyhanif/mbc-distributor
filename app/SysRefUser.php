<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class SysRefUser extends Model
{
    protected $table = 'sys_ref_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'nama',
        'id_distributor',
        'username',
        'password',
        'id_usergroup',
        'is_aktif'
    ];
}
