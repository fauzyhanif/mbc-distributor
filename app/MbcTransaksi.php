<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MbcTransaksi extends Model
{
    protected $table = "mbc_transaksi";
    protected $fiilable = [
        "id",
        "tgl_transaksi",
        "no_invoice",
        "id_distributor",
        "nama",
        "no_hp",
        "alamat",
        "id_kecamatan",
        "id_kota",
        "id_provinsi",
        "is_dropshipper",
        "dropship_nama",
        "dropship_no_hp",
        "dropship_alamat",
        "dropship_id_kecamatan",
        "dropship_id_kota",
        "dropship_id_provinsi",
        "logistik_name",
        "logistik_service",
        "logistik_day",
        "request_jtr",
        "konfirmasi_jtr",
        "ttl_ongkir",
        "ttl_berat",
        "ttl_belanja",
        "ttl_transaksi",
        "stts_umum",
        "stts_bayar",
        "stts_batal",
        "catatan",
        "subtraction_stock",
        "resi"
    ];
}
