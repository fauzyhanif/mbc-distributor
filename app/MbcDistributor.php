<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MbcDistributor extends Model
{
    protected $table = "mbc_distributor";
    protected $fiilable = [
        "nama",
        "no_hp",
        "alamat",
        "id_provinsi",
        "id_kota",
        "id_kecamatan",
        "is_aktif",
    ];
}
