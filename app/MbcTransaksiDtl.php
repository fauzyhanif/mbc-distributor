<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MbcTransaksiDtl extends Model
{
    protected $table = "mbc_transaksi_dtl";
    protected $fiilable = [
        "id",
        "no_invoice",
        "kode_produk",
        "qty",
        "harga",
        "jumlah"
    ];
}
