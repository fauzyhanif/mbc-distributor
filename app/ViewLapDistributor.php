<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewLapDistributor extends Model
{
    protected $table = "view_lap_distributor";
    protected $fiilable = [
        "id",
        "nama",
        "city_name",
        "jml_omset",
        "jml_qty",
        "order_day",
    ];
}
