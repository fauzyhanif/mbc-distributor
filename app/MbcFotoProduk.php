<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MbcFotoProduk extends Model
{
    protected $table = "mbc_foto_produk";
    protected $fiilable = [
        "id",
        "kode_produk",
        "nama_file"
    ];
}
