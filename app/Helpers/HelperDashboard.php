<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class HelperDashboard {
    public static function getTotalTransaksi()
    {
        $total = DB::table('mbc_transaksi')
                ->selectRaw('sum(ttl_transaksi) as total')
                ->where('stts_umum', '!=', '0')
                ->first();
        return $total->total;
    }

    public static function getTotalTransaksiToday()
    {
        $total = DB::table('mbc_transaksi')
                ->selectRaw('sum(ttl_transaksi) as total')
                ->where('stts_umum', '!=', '0')
                ->whereDate('tgl_transaksi', Carbon::today())
                ->first();
        return $total->total;
    }

    public static function getTotalTransaksiThisMonth()
    {
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');
        $thisMonth = date('Y-m');

        $total = DB::table('mbc_transaksi')
                ->selectRaw('sum(ttl_transaksi) as total')
                ->where('stts_umum', '!=', '0')
                ->where('tgl_transaksi', "like", "%$thisMonth%")
                // ->whereBetween('tgl_transaksi', [$firstDay, $lastDay])
                ->first();
        return $total->total;
    }

    public static function getTotalDistributor()
    {
        $total = DB::table('mbc_distributor')
                ->selectRaw('count(*) as total')
                ->first();
        return $total->total;
    }

    public static function getTotalKategoriProduk()
    {
        $total = DB::table('mbc_kategori_produk')
                ->selectRaw('count(*) as total')
                ->first();
        return $total->total;
    }

    public static function getTotalProduk()
    {
        $total = DB::table('mbc_produk')
                ->selectRaw('count(*) as total')
                ->first();
        return $total->total;
    }

    public static function getOmsetDistributor()
    {
        $datas = DB::table('mbc_transaksi')
                ->leftJoin('mbc_distributor', 'mbc_transaksi.id_distributor', '=', 'mbc_distributor.id')
                ->leftJoin('tb_ro_cities', 'mbc_distributor.id_kota', '=',  'tb_ro_cities.city_id')
                ->select('mbc_distributor.nama', 'tb_ro_cities.city_name',
                        DB::raw('SUM(mbc_transaksi.ttl_belanja) as jml_omset,
                        SUM(mbc_transaksi.ttl_qty) as jml_qty
                        '))
                ->where('mbc_transaksi.stts_umum', '!=', '0')
                ->orderBy('jml_omset', 'DESC')
                ->groupBy('mbc_transaksi.id_distributor', 'mbc_distributor.nama', 'tb_ro_cities.city_name')
                ->limit(5)
                ->get();

        return $datas;
    }

    public static function getLastTrx()
    {
        $datas = DB::table('mbc_transaksi as a')
                ->leftJoin('mbc_stts_transaksi as b', 'a.stts_umum', '=', 'b.kode')
                ->select('a.no_invoice', 'a.is_dropshipper', 'a.nama', 'a.ttl_transaksi', 'b.nama as status')
                ->orderBy('no_invoice', 'DESC')
                ->limit(5)
                ->get();

        return $datas;
    }
}
?>
