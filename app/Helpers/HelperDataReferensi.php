<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HelperDataReferensi {

    public static function konversiTgl($date, $date_format='')
    {
        $dayList = array(
            'Sunday'    => 'Minggu',
            'Monday'    => 'Senin',
            'Tuesday'   => 'Selasa',
            'Wednesday' => 'Rabu',
            'Thursday'  => 'Kamis',
            'Friday'    => 'Jumat',
            'Saturday'  => 'Sabtu'
        );

        $monthList = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );

        $format_hari = date('l', strtotime($date));
        $format_tgl  = date('d', strtotime($date));
        $format_bln  = date('m', strtotime($date));
        $format_thn  = date('Y', strtotime($date));

        switch ($date_format) {
            case 'l':
                # Hari ex: Kamis
                $output = $dayList[$format_hari];
                break;
            case 'd':
                # Tanggal ex: 21
                $output = $format_tgl;
                break;
            case 'm':
                # Bulan ex: Januari
                $output = $monthList[$format_bln];
                break;
            case 'y':
                # Tahun ex: 2016
                $output = $format_thn;
                break;
            case 'T':
                # Tgl Lahir
                $output = $format_tgl . ' ' . $monthList[$format_bln] . ' ' . $format_thn;
                break;
            default:
                # Hari, Tanggal-Bulan-Tahun ex: Rabu, 26-Juli-2016
                $output = $dayList[$format_hari] . ', ' . $format_tgl . ' ' . $monthList[$format_bln] . ' ' . $format_thn;
                break;
        }

        return $output;
    }

    public static function MyCart() {
        // yg dibutuhkan : jml item, data item
        $idDistributor = Session::get('id_distributor');
        $countMyCart = 0;
        $myCart = DB::table('mbc_keranjang as a')
                ->leftJoin('mbc_produk as b', 'a.kode_produk', '=', 'b.kode')
                ->select('a.harga', 'b.nama', 'b.thumbnail')
                ->where('id_distributor', '=', $idDistributor)
                ->get();

        $resultArray = [];
        $resultArray = [
            "countMyCart" => count($myCart),
            "myCart" => $myCart
        ];

        return $resultArray;
    }


}
