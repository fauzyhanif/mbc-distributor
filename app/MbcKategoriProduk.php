<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MbcKategoriProduk extends Model
{
    protected $table = "mbc_kategori_produk";
    protected $fiilable = [
        "id",
        "nama"
    ];
}
