<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ url('public/img/logo_mbc_family_v1.jpg') }}">
    <title>Muslimah Beauty Care | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{  url('public/v4/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{  url('public/v4/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- Select -->
    <link rel="stylesheet" href="{{ url('/public/assets/select2/dist/css/select2.min.css') }}">
    <!-- Daterangepicker -->
    <link rel="stylesheet" href="{{  url('public/v4/plugins/daterangepicker/daterangepicker.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{  url('public/v4/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    {{-- datatable --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="{{  url('public/v4/dist/css/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{  url('public/v4/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">


    <!-- jQuery -->
    <script src="{{ url('public/v4/plugins/jquery/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ url('public/v4/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ url('public/v4/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ url('public/v4/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ url('public/v4/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ url('public/v4/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    {{-- Datatable --}}
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select -->
    <script src="{{ url('/public/assets/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ url('public/v4/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{ url('public/v4/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ url('public/v4/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ url('public/v4/dist/js/pages/dashboard.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ url('public/v4/dist/js/demo.js') }}"></script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">{{ HelperDataReferensi::konversiTgl(date('Y-m-d'))  }}</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Pengaduan Developer (0823-2549-4207)</a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{ url('/logout') }}" class="nav-link">Logout</a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ url('/') }}" class="brand-link">
            <span class="brand-text font-weight-light">Muslimah Beauty Care</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                <img src="{{ url('/public/img/user.png') }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                <a href="#" class="d-block">{{ Session::get('username') }}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="{{ url('/') }}" class="nav-link {{ (request()->is('/')) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    @if (Session::get('usergroup_aktif') == '1' || Session::get('usergroup_aktif') == '99')

                    <li class="nav-header">REFERENSI</li>
                    <li class="nav-item">
                        <a href="{{ url('/user') }}" class="nav-link {{ (request()->is('user')) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-user"></i>
                            <p>User</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/admin/setting-web') }}" class="nav-link {{ (request()->is('setting-web')) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-sliders-h"></i>
                            <p>Setting Web</p>
                        </a>
                    </li>

                    <li class="nav-header">MENU UTAMA</li>
                    <li class="nav-item has-treeview {{ (request()->is('admin/distributor') or request()->is('admin/distributor/form-add')) ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-network-wired"></i>
                            <p>
                                Distributor
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ url('/admin/distributor') }}" class="nav-link {{ (request()->is('admin/distributor')) ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                <p>Distributor</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/admin/distributor/form-add') }}" class="nav-link {{ (request()->is('admin/distributor/form-add')) ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                <p>Tambah Distributor</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview {{ (request()->is('admin/kategori-produk') or request()->is('admin/kategori-produk/form-add')) ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-project-diagram"></i>
                            <p>
                                Kategori Produk
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ url('/admin/kategori-produk') }}" class="nav-link {{ (request()->is('admin/kategori-produk')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Kategori Produk</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/admin/kategori-produk/form-add') }}" class="nav-link {{ (request()->is('admin/kategori-produk/form-add')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tambah Kategori Produk</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview {{ (request()->is('admin/produk') or request()->is('admin/produk/form-add')) ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-cubes"></i>
                            <p>
                                Produk
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ url('/admin/produk') }}" class="nav-link {{ (request()->is('admin/produk')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Produk</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/admin/produk/form-add') }}" class="nav-link {{ (request()->is('admin/produk/form-add')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tambah Produk</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif

                    @if (Session::get('usergroup_aktif') == '1' || Session::get('usergroup_aktif') == '2' || Session::get('usergroup_aktif') == '99')
                    <li class="nav-header">TRANSAKSI</li>
                    <li class="nav-item">
                        <a href="{{ url('/admin/list-transaksi') }}" class="nav-link {{ (request()->is('admin/list-transaksi')) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-list"></i>
                            <p>Daftar Transaksi</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('/admin/transaksi/print-invoice-banyak') }}" class="nav-link {{ (request()->is('admin/print-invoice-banyak')) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-print"></i>
                            <p>Print Invoice (Banyak)</p>
                        </a>
                    </li>
                    @endif

                    @if (Session::get('usergroup_aktif') == '1' || Session::get('usergroup_aktif') == '99')
                    <li class="nav-header">LAPORAN</li>
                    {{-- <li class="nav-item">
                        <a href="{{ url('/admin/laporan/laba-kategori-produk') }}" class="nav-link {{ (request()->is('admin/laporan/laba-kategori-produk')) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-file-alt"></i>
                            <p>Laba per Kategori Produk</p>
                        </a>
                    </li> --}}
                    <li class="nav-item">
                        <a href="{{ url('/admin/laporan/laba-produk') }}" class="nav-link {{ (request()->is('admin/laporan/laba-produk')) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-file-alt"></i>
                            <p>Laba per Produk</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/admin/laporan/distributor') }}" class="nav-link {{ (request()->is('admin/laporan/distributor')) ? 'active' : '' }}">
                            <i class="nav-icon fas fa-file-alt"></i>
                            <p>Omset Distributor</p>
                        </a>
                    </li>
                    @endif
                </ul>
            </nav>
        </div>
    </aside>
