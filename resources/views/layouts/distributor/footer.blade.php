    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="container">
            <strong>Copyright &copy; <a href="https://fauzyhanif.github.io/Portofolio/">Fauzy Hanif</a>.</strong> <b>Version</b> 1.0 Beta
        </div>
        <!-- /.container -->
    </footer>
</div>
<!-- ./wrapper -->


<script>
    $(function () {
        $('.datemask').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' });
        //Initialize Select2 Elements
        $('.select2').select2();

        // bootstrap switch
        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });
    });
</script>
</body>
</html>
