<?php use Illuminate\Support\Facades\Session; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Muslimah Beauty Care</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ url('public/v4/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('public/v4/dist/css/adminlte.min.css') }}">
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    {{-- Custom css --}}
    @include('assets.css.custom')
    {{-- Toastr --}}
    <link rel="stylesheet" href="{{ url('public/v4/plugins/toastr/toastr.css') }}">

    <!-- jQuery -->
    <script src="{{ url('public/v4/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ url('public/v4/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    {{-- Toastr --}}
    <script src="{{ url('public/v4/plugins/toastr/toastr.min.js') }}"></script>
    <!-- Bootstrap Switch -->
    <script src="{{ url('public/v4/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ url('public/v4/dist/js/adminlte.min.js') }}"></script>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition layout-top-nav">
<div class="loading-get-cost" style="display: none">
    <br><br><br><br><br><br><br><center><h3>Mohon tunggu, proses get ongkir sedang berlangsung.</h3></center>
</div>
<div class="wrapper">

    <nav class="main-header navbar navbar-expand-md navbar-light navbar-pink fixed-top">
        <div class="container">
            <a href="{{ url('/dist/dashboard') }}" class="navbar-brand">
                <img src="{{ url('public/img/logo_mbc_family_v1.png') }}" alt="logo" style="width: 40px">
                {{-- <span class="brand-text cst-brand-text font-weight-bold">MBC</span> --}}
            </a>

            <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="{{ url('/dist/dashboard') }}" class="nav-link cst-nav-link">Dashboard</a>
                    </li>
                    <li class="nav-item">
                    <a href="{{ url('/dist/produk') }}" class="nav-link cst-nav-link">Produk</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/dist/keranjang') }}" class="nav-link cst-nav-link">Kranjang</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/dist/pesanan') }}" class="nav-link cst-nav-link">Pesanan Saya</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/logout') }}" class="nav-link cst-nav-link">Logout</a>
                    </li>
                </ul>
            </div>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <!-- Messages Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link cst-nav-link" data-toggle="dropdown" href="#">
                    <i class="fas fa-shopping-cart"></i>
                    <span class="badge badge-warning navbar-badge header-count-cart">
                        {{ HelperDataReferensi::MyCart()['countMyCart'] }}
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="#" class="dropdown-item header-list-cart">
                        <!-- Keranjang Start -->
                        @if (HelperDataReferensi::MyCart()['countMyCart'] == 0)
                            <div class="media my-2">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Keranjang kamu masih kosong.
                                    </h3>
                                </div>
                            </div>
                        @else
                            @foreach (HelperDataReferensi::MyCart()['myCart'] as $item)
                                <div class="media my-2">
                                    <img src="{{ url('foto-produk', $item->thumbnail) }}" alt="User Avatar" class="mr-3 img-circle" style="width: 37px;">
                                    <div class="media-body">
                                        <h3 class="dropdown-item-title">
                                            {{ Str::limit($item->nama, 20) }}
                                        </h3>
                                        <p class="text-sm text-muted">@currency($item->harga),-</p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </a>

                    <div class="dropdown-divider"></div>
                    @if (HelperDataReferensi::MyCart()['countMyCart'] == 0)
                        <a href="{{ url('dist/produk') }}" class="dropdown-item dropdown-footer bottom-of-header-cart">Mulai Belanja Yuk!</a>
                    @else
                        <a href="{{ url('dist/keranjang') }}" class="dropdown-item dropdown-footer bottom-of-header-cart">Lihat Semua</a>
                    @endif
                </div>
            </li>

            <li class="nav-item">
                <button class="navbar-toggler navbar-light order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </li>
        </ul>
        </div>
    </nav>
