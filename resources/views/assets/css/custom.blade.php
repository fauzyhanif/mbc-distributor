<style>
.btn-pink {
    color: #ffffff;
    background-color: #FF8CB6;
    border-color: #FF8CB6;
    box-shadow: none;
}

.btn-pink:hover {
  color: #ffffff;
  background-color: #F64F8C;
  border-color: #F64F8C;
}

.btn-outline-pink {
  color: #FF8CB6;
  border-color: #FF8CB6;
}

.btn-outline-pink:hover {
  color: #ffffff;
  background-color: #F64F8C;
  border-color: #F64F8C;
}

.cst-brand-text {
    color: #fff;
}

.cst-nav-link {
    color: #fff !important;
}

.navbar-pink {
    background-color: #FF8CB6 !important;
}

.content-header {
    margin-top: 67px !important;
}

</style>
