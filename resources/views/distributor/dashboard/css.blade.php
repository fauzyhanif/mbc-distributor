<style>
    @media (max-width: 768px) {
        .profile-user-img {
            width: 60px !important;
        }

        .dash-profile-name {
            margin-top: 8px !important;
        }

        .dash-profile-name-text {
            font-size: 18px;
        }

        .dash-profile-name-origin {
            font-size: 14px;
        }

        .dt-list-pesanan {
            font-size: 16px !important;
        }
    }
</style>
