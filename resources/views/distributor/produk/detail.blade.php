@extends('distributor')

@section('content')
@csrf
<section class="content-header" style="margin-top: 70px;">
    <h4>
        <a href="{{ url('dist/produk') }}" style="color: grey">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a>
        &nbsp;&nbsp;
        Detail Produk
    </h4>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <!-- Default box -->
                <div class="card card-solid">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($dtFoto as $item)
                                        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $no++ }}"></li>
                                        @endforeach
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active" style="height: 300px">
                                            <img class="d-block w-100" src="{{ url('foto-produk', $data->thumbnail) }}" alt="First slide" style="height: 100%">
                                        </div>
                                        @foreach ($dtFoto as $item)
                                        <div class="carousel-item" style="height: 300px">
                                            <img class="d-block w-100" src="{{ url('foto-produk', $item->nama_file) }}" alt="Second slide" style="height: 100%">
                                        </div>
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                  </div>
                            </div>

                            <div class="col-12 col-sm-6">
                                <h3 class="my-3 font-weight-normal">{{ $data->nama }}</h3>
                                <p class="text-muted">{{ $data->deskripsi }}</p>

                                <hr>
                                <div class="mt-4">
                                    <h3 class="mb-0">
                                        Rp. <?= number_format($data->harga_jual,2,',','.'); ?>
                                    </h3>
                                </div>

                                <div class="mt-4">
                                    @if ($data->stok <= 0)
                                        <p class="text-muted text-center">Habis!</p>
                                    @else
                                        <button class="btn btn-pink btn-sm btn-block" onclick="addToCart('{{ $data->kode }}')">
                                            <i class="fas fa-cart-plus"></i> TAMBAHKAN
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('distributor.produk.js')
@endsection
