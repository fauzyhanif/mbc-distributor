@extends('distributor')
@section('content')

@php $weight = 0; @endphp
@foreach ($myCart as $item)
    @php $weight = $weight + ($item->berat * $item->qty); @endphp
@endforeach

<section class="content-header" style="margin-top: 70px;">
    <h4>
        <a href="{{ url('dist/keranjang') }}" style="color: grey">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a>
        &nbsp;&nbsp;
        Checkout
    </h4>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <h5 class="text-muted float-left mr-2">Kirim sebagai Dropshipper?</h5>
                        <input type="checkbox" name="my-checkbox" id="switch-dropshipper" class="float-left"
                            checked
                            data-bootstrap-switch
                            data-off-text="Yes"
                            data-on-text="No"
                            data-off-color="secondary"
                            data-on-color="light">
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-secondary"><i class="fas fa-map-marker-alt"></i>&nbsp; Alamat Pengiriman</h3>
                            </div>
                            <div class="card-body default-address" style="display: block">
                                <span class="text-muted">
                                    {{ $myProfile->nama . ' (' . $myProfile->no_hp . ')' }}  <br>
                                    {{ $myProfile->alamat . ', Kec. ' . $myProfile->subdistrict_name . ', ' . $myProfile->city_name . ', ' . $myProfile->province_name }}
                                </span>
                            </div>

                            <div class="card-body dropship-address" style="display: none">
                                <form action="{{ url('/dist/checkout/get-cost-dropshipper') }}" method="POST" data-remote>

                                    {{-- hidden data weight --}}
                                    <input type="hidden" name="weight" value="{{ $weight }}">
                                    @csrf

                                    <div class="form-group">
                                        <label class="alert alert-info">
                                            Silahkan isi nama, nomor handphone dan alamat lengkap pengiriman dropshipper dibawah ini.
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label>Nama <span class="text-red">*</span></label>
                                        <input type="text" name="dropship_nama" class="form-control" required placeholder="Ex: Jhon Doe">
                                    </div>

                                    <div class="form-group">
                                        <label>No. Handphone <span class="text-red">*</span></label>
                                        <input type="text" name="dropship_no_hp" class="form-control" required placeholder="Ex: 089XXXXXXXX">
                                    </div>

                                    <div class="form-group">
                                        <label>Alamat lengkap <span class="text-red">*</span></label>
                                        <textarea name="dropship_alamat" class="form-control" required></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Provinsi <span class="text-red">*</span></label>
                                        <select name="dropship_id_provinsi" class="form-control" required onchange="cariKota(this.value)">
                                            <option value="">-- Pilih Provinsi --</option>
                                            @foreach ($provinces as $province)
                                            <option value="{{ $province->province_id }}">{{ $province->province_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Kota/Kabupaten <span class="text-red">*</span></label>
                                        <select name="dropship_id_kota" class="form-control" id="kota" required onchange="cariKecamatan(this.value)">
                                            <option value="">Pilih provinsi terlebih dahulu</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Kecamatan <span class="text-red">*</span></label>
                                        <select name="id_kecamatan" class="form-control" id="kecamatan" required>
                                            <option value="">Pilih kota/kabuptaen terlebih dahulu</option>
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-sm btn-warning btn-block">KONFIRMASI</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    @if ($weight > 5000)
                        <div class="col-md-12 alert-request-jtr">
                            <div class="alert alert-info alert-dismissible">
                                <h5 class="font-weight-bold">Mau pengiriman lebih murah? </h5>
                                Paket kamu lebih dari 5kg, kamu bisa pakai <b>Logistik JNE servis JTR</b>
                                degan cara
                                <b><a href="#" data-toggle="modal" data-target="#modal-request-jtr">Klik disini</a></b>.
                            </div>
                        </div>
                    @endif

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-secondary"><i class="fas fa-truck"></i>&nbsp; Opsi Pengiriman</h3>
                            </div>

                            <div class="card-body wrap-logistic">
                                <div class="logistic-list">
                                    @php
                                        $no = 1;
                                        $noForService = 1;
                                    @endphp
                                    @foreach ($costs as $cost)

                                        <button class="btn btn-default btn-block btn-flat mb-2" type="button"
                                            data-toggle="collapse" data-target="#collapse{{ $no }}" aria-expanded="false"
                                            aria-controls="collapseExample" style="text-align: left">
                                            {{ $cost->name }} <span class="text-muted"></span>
                                        </button>

                                        <div class="collapse" id="collapse{{ $no }}">
                                            <div class="card card-body">
                                                @foreach ($cost->costs as $service)
                                                    <div class="form-group border">
                                                        <div class="custom-control custom-radio my-3 mx-2">
                                                            <input
                                                                class="custom-control-input radio-cost"
                                                                type="radio"
                                                                id="customRadio{{ $noForService }}"
                                                                name="customRadio[]"
                                                                value="{{ $cost->name . ' * ' . $service->description . ' * ' . $service->cost[0]->etd . ' * ' . $service->cost[0]->value }}"
                                                            >
                                                            <label for="customRadio{{ $noForService }}" class="custom-control-label">{{ $service->description }}</label><br>
                                                            <span class="text-muted text-right">
                                                                {{ $service->cost[0]->etd }} hari - <span class="pull-right">@currency($service->cost[0]->value)</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                @php $noForService = $noForService + 1; @endphp
                                                @endforeach
                                            </div>
                                        </div>

                                    @php $no = $no + 1; @endphp
                                    @endforeach

                                    {{-- COD --}}
                                    @if (Session::get('id_distributor') == '14')
                                        <button class="btn btn-default btn-block btn-flat mb-2" type="button"
                                            data-toggle="collapse" data-target="#collapseCOD" aria-expanded="false"
                                            aria-controls="collapseExample" style="text-align: left">
                                            COD <span class="text-muted"></span>
                                        </button>                                        
                                        
                                        <div class="collapse" id="collapseCOD">
                                            <div class="card card-body">
                                                <div class="form-group border">
                                                    <div class="custom-control custom-radio my-3 mx-2">
                                                        <input
                                                            class="custom-control-input radio-cost"
                                                            type="radio"
                                                            id="customRadioCOD"
                                                            name="customRadio[]"
                                                            value="COD * COD Tanpa Ongkir * 0 * 0"
                                                        >
                                                        <label for="customRadioCOD" class="custom-control-label">COD Tanpa Ongkir</label><br>
                                                        <span class="text-muted text-right">
                                                            Ambil di warehaouse</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    {{-- COD --}}
                                </div>
                            </div>

                            <div class="logistic-alert" style="display: none;">
                                <h4 class="text-center">SILAHKAN KLIK TOMBOL KONFIRMASI DIATAS TERLEBIH DAHULU</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-secondary"><i class="fas fa-edit"></i>&nbsp; Catatan</h3>
                            </div>
                            <div class="card-body">
                                <textarea name="catatan" class="form-control" placeholder="Berikan catatan untuk seller disini"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title text-secondary">
                            <i class="fas fa-cubes"></i>&nbsp; Daftar Pesanan
                        </h3>
                    </div>
                    <div class="card-body mt-0" style="padding-top: 0px; padding-bottom: 0px">
                        <div class="cart-list">
                        @php
                            $shoppingAmount = 0;
                            $itemAmount = 0;
                            $weightAmount = 0;
                        @endphp
                        @foreach ($myCart as $item)
                            <div class="border-bottom">
                                <div class="row my-3">
                                    <div class="col-md-7">
                                        <h5 class="font-weight-normal text-dark">{{ $item->nama }}</h5>
                                    </div>
                                    <div class="col-md-3 col-6">
                                        <span class="font-weight-light text-muted">{{ $item->qty }} x @currency($item->harga)</span>
                                    </div>
                                    <div class="col-md-2 col-6 text-right">
                                        <span class="font-weight-normal text-dark">@currency($item->jumlah)</span>
                                    </div>
                                </div>
                            </div>
                        @php
                            $shoppingAmount = $shoppingAmount + $item->jumlah;
                            $itemAmount = $itemAmount + $item->qty;
                            $weightAmount = $weightAmount + ($item->berat * $item->qty);
                        @endphp
                        @endforeach
                        </div>


                        <div class="row my-3">
                            <div class="col-md-6 col-6">
                                <span class="text-muted font-weight-light">Berat </span>
                            </div>
                            <div class="col-md-6 col-6 text-right">
                                <span class="font-weight-medium">
                                    @php
                                        echo $weightAmount / 1000 . " kg";
                                    @endphp
                                </span>
                            </div>
                            <div class="col-md-6 col-6">
                                <span class="text-muted font-weight-light">Total Item</span>
                            </div>
                            <div class="col-md-6 col-6 text-right">
                            <span class="font-weight-medium">{{ $itemAmount }} pcs</span>
                            </div>
                            <div class="col-md-6 col-6">
                                <span class="text-muted font-weight-light">Subtotal untuk Produk</span>
                            </div>
                            <div class="col-md-6 col-6 text-right">
                                <span class="font-weight-medium">@currency($shoppingAmount)</span>
                            </div>
                            <div class="col-md-6 col-6">
                                <span class="text-muted font-weight-light">Subtotal Pengiriman</span>
                            </div>
                            <div class="col-md-6 col-6 text-right">
                                <span class="font-weight-medium cost-subtotal">Silahkan pilih pengiriman</span>
                            </div>

                            <div class="col-md-6 col-6">
                                <span class="font-weight-bold">Total Tagihan</span>
                            </div>
                            <div class="col-md-6 col-6 text-right">
                                <span class="font-weight-bold amount">@currency($shoppingAmount)</span>
                            </div>
                        </div>

                        <div class="row my-3">
                            <button class="btn btn-pink btn-sn btn-block" onclick="createOrder()">
                                BUAT PESANAN
                            </button>
                            <a href="{{ url('/dist/produk') }}" class="btn btn-outline-pink btn-sn btn-block">
                                BELANJA LAGI
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

{{-- hidden data --}}
@csrf
<input type="hidden" name="default_id_kecamatan" value="{{ $myProfile->id_kecamatan }}">
<input type="hidden" name="logistik" value="0">
<input type="hidden" name="is_dropshipper" value="N">
<input type="hidden" name="ttl_belanja" value="{{ $shoppingAmount }}">
{{-- hidden data --}}

@include('distributor.checkout.modalRequestJtr')
@include('distributor.checkout.js')
@endsection
