<script>
$(function () {
    // bootstrap switch
    $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "2000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
})

$('.radio-cost').click(function(){
    var datas = this.value.split(' * ');
    var name = datas[0];
    var description = datas[1];
    var day = datas[2];
    var cost = datas[3];
    //
    $('input[name="logistik"]').val(this.value)

    // sum total
    sumTotal(cost);

    // append to view
    cost = formatRupiah(cost);
    $('.cost-subtotal').text("Rp. " + cost)

});

function sumTotal(costAmount) {
    var shoppingAmount = $('input[name="ttl_belanja"]').val();
    var amount = 0;
    amount = parseFloat(shoppingAmount) + parseFloat(costAmount);
    amount = formatRupiah(amount);

    // append to view
    $('.amount').text("Rp. " + amount)
}

function formatRupiah(bilangan){
    var	number_string = bilangan.toString(),
    sisa 	= number_string.length % 3,
    rupiah 	= number_string.substr(0, sisa),
    ribuan 	= number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    return rupiah;
}

function createOrder() {
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/check-session-with-js')) !!},
        dataType: "JSON",
        success: function(res){
            if (res != null) {
                var logistik = $("input[name='logistik']").val();
                if (logistik == '0') {
                    toastr.error("Silahkan pilih jasa pengiriman");
                } else {
                    var url = {!! json_encode(url('/dist/checkout/buat-pesanan')) !!};
                    var data = {
                        "_token" : $("input[name='_token']").val(),
                        "logistik" : $("input[name='logistik']").val(),
                        "catatan" : $("textarea[name='catatan']").val(),
                        "is_dropshipper" : $("input[name='is_dropshipper']").val(),
                        "dropship_nama" : $("input[name='dropship_nama']").val(),
                        "dropship_no_hp" : $("input[name='dropship_no_hp']").val(),
                        "dropship_alamat" : $("textarea[name='dropship_alamat']").val(),
                        "dropship_id_kecamatan" : $("select[name='id_kecamatan']").val(),
                        "dropship_id_kota" : $("select[name='dropship_id_kota']").val(),
                        "dropship_id_provinsi" : $("select[name='dropship_id_provinsi']").val(),
                    };

                    $.ajax({
                        type: 'POST',
                        url: url,
                        dataType: "JSON",
                        data: data,
                        beforeSend: function() {
                            var htmlLoading = "<br><br><br><br><br><br><br><center><h1>Mohon tunggu, proses Pembuatan Pesanan sedang berlangsung</h1></center>";
                            $('body').html(htmlLoading);
                        },
                        success: function(res){
                            var base = {!! json_encode(url('/dist/pembayaran')) !!};
                            var urlReload = base + "/" + res;

                            //this will redirect us in same window
                            document.location.href = urlReload;
                        }
                    });
                }
            } else {
                // redirect to login ulang page
                var base = {!! json_encode(url('/login-ulang')) !!};
                document.location.href = base;
            }
        }
    })
}

function requestJtr() {
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/check-session-with-js')) !!},
        dataType: "JSON",
        success: function(res){
            if (res != null) {

                var url = {!! json_encode(url('/dist/checkout/request-jtr')) !!};
                var data = {
                    "_token" : $("input[name='_token']").val(),
                    "catatan" : $("textarea[name='catatan']").val(),
                    "is_dropshipper" : $("input[name='is_dropshipper']").val(),
                    "dropship_nama" : $("input[name='dropship_nama']").val(),
                    "dropship_no_hp" : $("input[name='dropship_no_hp']").val(),
                    "dropship_alamat" : $("textarea[name='dropship_alamat']").val(),
                    "dropship_id_kecamatan" : $("select[name='id_kecamatan']").val(),
                    "dropship_id_kota" : $("select[name='dropship_id_kota']").val(),
                    "dropship_id_provinsi" : $("select[name='dropship_id_provinsi']").val(),
                };

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: "JSON",
                    data: data,
                    beforeSend: function() {
                        var htmlLoading = "<br><br><br><br><br><br><br><center><h1>Mohon tunggu, proses Pembuatan Pesanan sedang berlangsung</h1></center>";
                        $('body').html(htmlLoading);
                    },
                    success: function(res){
                        var base = {!! json_encode(url('/dist/checkout/waiting-request-jtr')) !!};

                        //this will redirect us in same window
                        document.location.href = base + '/' + res;
                    }
                });
            } else {
                // redirect to login ulang page
                var base = {!! json_encode(url('/login-ulang')) !!};
                document.location.href = base;
            }
        }
    })
}

$('#switch-dropshipper').on('switchChange.bootstrapSwitch', function (event, state) {
    var x = $(this).data('on-text');
    var y = $(this).data('off-text');
    if($("#switch-dropshipper").is(':checked')) {
        $('input[name="is_dropshipper"]').val("N");
        $('.dropship-address').css("display", "none");
        $('.default-address').css("display", "block");
        $('.alert-request-jtr').css("display", "block");
        showDefaultLogistic();
    } else {
        $('input[name="is_dropshipper"]').val("Y");
        $('.dropship-address').css("display", "block");
        $('.default-address').css("display", "none");
        $('.alert-request-jtr').css("display", "none");
        hiddenLogistic();
    }
});

function hiddenLogistic() {
    var html = $('.logistic-alert').html();
    $('.wrap-logistic').html(html);
}

function showDefaultLogistic() {
    var url = {!! json_encode(url('/dist/checkout/get-cost-dropshipper')) !!};
    var data = {
        "_token" : $("input[name='_token']").val(),
        "id_kecamatan" : $("input[name='default_id_kecamatan']").val(),
        "weight" : $("input[name='weight']").val()
    };

    $.ajax({
        type: 'POST',
        url: url,
        dataType:'html',
        data: data,
        beforeSend: function() {
            $('.loading-get-cost').css("display", "block");
            $('.wrapper').css("display", "none");
        },
        success: function( res ){
            $('.loading-get-cost').css("display", "none");
            $('.wrapper').css("display", "block");
            $('.wrap-logistic').html(res);
        }
    });
}

function cariKota(IdProvinsi) {
    hiddenLogistic()
    var base = {!! json_encode(url('/helpers/cari-kota')) !!};
    var url = base + "/" + IdProvinsi;
    $.ajax({
        type: "GET",
        url: url,
        dataType : 'json',
        success: function(result){
            var y = '<option value="">-- Pili Kota/Kabupaten --</option>'
            for (var x in result){
                y += '<option value="'+result[x].city_id+'">'+result[x].city_name+'</option>'
            }

            $('#kota').html(y)

        }
    });
}

function cariKecamatan(IdKota) {
    hiddenLogistic()
    var base = {!! json_encode(url('/helpers/cari-kecamatan')) !!};
    var url = base + "/" + IdKota;
    $.ajax({
        type: "GET",
        url: url,
        dataType : 'json',
        success: function(result){
            var y = '<option value="">-- Pilih Kecamatan --</option>'
            for (var x in result){
                y += '<option value="'+result[x].subdistrict_id+'">'+result[x].subdistrict_name+'</option>'
            }

            $('#kecamatan').html(y)

        }
    });
}

(function() {
    $('form[data-remote]').on('submit', function(e) {
        hiddenLogistic();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'html',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $('.loading-get-cost').css("display", "block");
                $('.wrapper').css("display", "none");
            },
            success: function( res ){
                $('.loading-get-cost').css("display", "none");
                $('.wrapper').css("display", "block");
                $('.alert-request-jtr').css("display", "block");
                $('.wrap-logistic').html(res);
            }
        });
        e.preventDefault();
    });
})();
</script>
