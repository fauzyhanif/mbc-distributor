<div class="col-md-12">
    <label class="alert alert-secondary">
        Silahkan pilih opsi pengiriman.
    </label>
</div>

@php
    $no = 1;
    $noForService = 1;
@endphp
@foreach ($costs as $cost)

    <button class="btn btn-default btn-block btn-flat mb-2" type="button"
        data-toggle="collapse" data-target="#collapse{{ $no }}" aria-expanded="false"
        aria-controls="collapseExample" style="text-align: left">
        {{ $cost->name }} <span class="text-muted"></span>
    </button>

    <div class="collapse" id="collapse{{ $no }}">
        <div class="card card-body">
            @foreach ($cost->costs as $service)
                <div class="form-group border">
                    <div class="custom-control custom-radio my-3 mx-2">
                        <input
                            class="custom-control-input radio-cost"
                            type="radio"
                            id="customRadio{{ $noForService }}"
                            name="customRadio[]"
                            value="{{ $cost->name . ' * ' . $service->description . ' * ' . $service->cost[0]->etd . ' * ' . $service->cost[0]->value }}"
                        >
                        <label for="customRadio{{ $noForService }}" class="custom-control-label">{{ $service->description }}</label><br>
                        <span class="text-muted text-right">
                            {{ $service->cost[0]->etd }} hari - <span class="pull-right">@currency($service->cost[0]->value)</span>
                        </span>
                    </div>
                </div>
            @php $noForService = $noForService + 1; @endphp
            @endforeach
        </div>
    </div>

@php $no = $no + 1; @endphp
@endforeach

{{-- COD --}}
@if (Session::get('id_distributor') == '14')
    <button class="btn btn-default btn-block btn-flat mb-2" type="button"
        data-toggle="collapse" data-target="#collapseCOD" aria-expanded="false"
        aria-controls="collapseExample" style="text-align: left">
        COD <span class="text-muted"></span>
    </button>                                        

    <div class="collapse" id="collapseCOD">
        <div class="card card-body">
            <div class="form-group border">
                <div class="custom-control custom-radio my-3 mx-2">
                    <input
                        class="custom-control-input radio-cost"
                        type="radio"
                        id="customRadioCOD"
                        name="customRadio[]"
                        value="COD * COD Tanpa Ongkir * 0 * 0"
                    >
                    <label for="customRadioCOD" class="custom-control-label">COD Tanpa Ongkir</label><br>
                    <span class="text-muted text-right">
                        Ambil di warehaouse</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
@endif
{{-- COD --}}

<script>
$(function () {
    // bootstrap switch
    $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
})

$('.radio-cost').click(function(){
    var datas = this.value.split(' * ');
    var name = datas[0];
    var description = datas[1];
    var day = datas[2];
    var cost = datas[3];

    $('input[name="logistik"]').val(this.value)

    // sum total
    sumTotal(cost);

    // append to view
    cost = formatRupiah(cost);
    $('.cost-subtotal').text("Rp. " + cost)

});

function sumTotal(costAmount) {
    var shoppingAmount = $('input[name="ttl_belanja"]').val();
    var amount = 0;
    amount = parseFloat(shoppingAmount) + parseFloat(costAmount);
    amount = formatRupiah(amount);

    // append to view
    $('.amount').text("Rp. " + amount)
}

function formatRupiah(bilangan){
    var	number_string = bilangan.toString(),
    sisa 	= number_string.length % 3,
    rupiah 	= number_string.substr(0, sisa),
    ribuan 	= number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    return rupiah;
}

</script>

