@extends('distributor')

@section('content')
<section class="content-header" style="margin-top: 70px;">
    <h1>
        Pembayaran
    </h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">


                        <div class="row my-2 text-center">
                            <div class="col-md-12">
                                <span class="text-muted">No Invoice Anda</span>
                                <h2>
                                    <label class="badge badge-light" id="no-invoice">{{ $invoice->no_invoice }}</label>
                                    <button class="btn btn-secondary btn-sm" onclick="copyToClipboard('#no-invoice')">
                                        Copy
                                    </button>
                                </h2>
                            </div>
                        </div>

                        <div class="row my-2 text-center">
                            <div class="col-md-12">
                                <span class="text-muted">Tagihan Anda Sebesar</span>

                                <h4 class="font-weight-bold">
                                    @currency($invoice->ttl_transaksi)
                                </h4>
                            </div>
                        </div>

                        <div class="row mt-2 mb-1 text-center">
                            <div class="col-md-12">
                                <span class="text-muted">Silahkan lakukan pembayaran dengan transfer ke Rekening berikut </span>
                            </div>
                        </div>

                        <div class="row text-center">
                            <div class="col-md-12 mb-1">
                                <h5>
                                    Bank BCA <br>
                                    A/N PT. Fatih Alghifary Indonesia <br>
                                    <h2>
                                        <label class="badge badge-light" id="rekening">0555112071</label>
                                        <button class="btn btn-secondary btn-sm" onclick="copyToClipboard('#rekening')">
                                            Copy
                                        </button>
                                    </h2>
                                </h5>
                            </div>
                        </div>

                        <div class="row mt-2 mb-1 text-center">
                            <div class="col-md-12">
                                <span class="text-muted">Kemudian konfirmasi pembayaran dengan mengirim bukti pembayaran dan disertakan No. Invoice ke </span>

                                <h5 class="mt-2">
                                    085320707984 (Ara) <br>
                                    081316896710 (Mardhyya)
                                </h5>
                            </div>
                        </div>

                        <div class="row mt-2 mb-1 text-center">
                            <div class="col-md-12">
                                <a href="{{ url('dist/dashboard') }}" class="text-pink" style="text-decoration: underline">Kembali ke Dashboard</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}
</script>

@endsection
