<script>
$('input[name="qty"]').keyup(function() {

    if ($(this).val() != '') {
        // get data
        var row = $(this).closest(".wrap-list-cart");
        var qty = $(this).val();
        var harga = row.find('input[name="harga"]').val();
        var idCart = row.find('input[name="id"]').val();

        // sum amount this good
        var resultThisGood = qty * harga;
        row.find('input[name="jumlah"]').val(resultThisGood);

        // sum amount
        sumAmount();

        // change data on db
        changeDataOnDb(idCart, qty, resultThisGood);
    }
});

$('.remove-cart').click(function(e) {
    $(this).closest(".wrap-list-cart").remove();
    e.preventDefault();

    // sum amount
    sumAmount();

    // get data
    var row = $(this).closest(".wrap-list-cart");
    var idCart = row.find('input[name="id"]').val();

    // remove data on db
    removeDataOnDb(idCart);
});

function sumAmount() {

    // sum amount shopping
    var shoppingAmount = 0;
    $('input[name="jumlah"]').each(function(){
        shoppingAmount += parseFloat(this.value);
    });

    // sum amount items
    var itemAmount = 0;
    $('input[name="qty"]').each(function(){
        itemAmount += parseFloat(this.value);
    });

    // append to view
    shoppingAmount = formatRupiah(shoppingAmount);
    $('.shopping-amount').text("Rp. " + shoppingAmount);
    $('.item-amount').text(itemAmount + " pcs");
}

function changeDataOnDb(idCart, qty, resultThisGood) {
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/check-session-with-js')) !!},
        dataType: "JSON",
        success: function(res){
            if (res != null) {
                var url = {!! json_encode(url('/dist/keranjang/ubah-item')) !!};
                var data = {
                    "_token" : $("input[name='_token']").val(),
                    "id" : idCart,
                    "qty" : qty,
                    "jumlah" : resultThisGood
                };

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: "JSON",
                    data: data,
                    success: function(res){}
                });
            } else {
                // redirect to login ulang page
                var base = {!! json_encode(url('/login-ulang')) !!};
                document.location.href = base;
            }
        }
    })
}

function removeDataOnDb(idCart) {
    $.ajax({
        type: 'GET',
        url: {!! json_encode(url('/check-session-with-js')) !!},
        dataType: "JSON",
        success: function(res){
            if (res != null) {
                var url = {!! json_encode(url('/dist/keranjang/hapus-item')) !!};
                var data = {
                    "_token" : $("input[name='_token']").val(),
                    "id" : idCart
                };

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: "JSON",
                    data: data,
                    success: function(res){

                    }
                });
            } else {
                // redirect to login ulang page
                var base = {!! json_encode(url('/login-ulang')) !!};
                document.location.href = base;
            }
        }
    })
}

function formatRupiah(bilangan){
    var	number_string = bilangan.toString(),
    sisa 	= number_string.length % 3,
    rupiah 	= number_string.substr(0, sisa),
    ribuan 	= number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    return rupiah;
}
</script>
