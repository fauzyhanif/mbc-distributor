@extends('distributor')

@section('content')

@include('distributor.pesanan.css')

<section class="content-header" style="margin-top: 70px;">
    <h4>
        <a href="{{ url('dist/dashboard') }}" style="color: black">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a>
        &nbsp;&nbsp;
        Pesanan Saya
    </h4>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row wrap-category">
            <div class="col-md-12">
                <div class="scrollmenu">
                    @foreach ($status as $item)
                        <a href="{{ url('/dist/pesanan', $item->kode) }}" class="{{ $item->kode == $sttsUmum ? 'active' : '' }}">{{ $item->nama }}</a>
                    @endforeach
                </div>
            </div>
        </div>

        @if (count($listOrder) == 0)
            <div class="mt-5 text-center">
                <h4 class="text-muted">
                    Tidak ada pesanan.
                </h4>
                <h4 class="text-dark">
                    <a href="{{ url('/dist/produk') }}" class="text-pink">
                        Mulai Belanja Yuk!
                    </a>
                </h4>
            </div>
        @else
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body" style="padding-top: 0px; padding-bottom: 0px;">
                            @foreach ($listOrder as $order)
                                <div class="row list-pesanan border-bottom py-2">
                                    <div class="col-md-6 col-6">
                                        <a href="{{ url('/dist/pesanan/detail', $order->no_invoice) }}" style="color: #e83e8c">
                                            <h4 class="dt-list-pesanan">{{ $order->no_invoice }}</h4>
                                        </a>
                                        <span class="font-weight-light text-muted">{{ HelperDataReferensi::konversiTgl($order->tgl_transaksi, 'T') }}</span>
                                        @if ($order->is_dropshipper == "Y")
                                            <label class="badge badge-warning">Dropship</label>
                                        @endif

                                        @if ($order->request_jtr == "Y")
                                            @if ($order->request_jtr == "Y" and $order->konfirmasi_jtr == "Y")
                                                <label class="badge badge-info">JTR berhasil dikonfirmasi</label>
                                            @else
                                                <label class="badge badge-info">Request JTR</label>
                                            @endif
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-6 text-right">
                                        <h4 class="font-weight-normal text-dark dt-list-pesanan">@currency($order->ttl_transaksi)</h4>
                                    <span class="font-weight-light text-muted">{{ $order->status }}</span>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        {{ $listOrder->links() }}
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>
@endsection
