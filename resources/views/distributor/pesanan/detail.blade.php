@extends('distributor')

@section('content')
<section class="content-header" style="margin-top: 70px;">
    <h4>
        <a href="{{ url('dist/pesanan') }}" style="color: grey">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a>
        &nbsp;&nbsp;
        Detail Pesanan
    </h4>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if ($transaction->request_jtr == 'Y' && $transaction->konfirmasi_jtr == 'N')
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title text-muted">
                                <i class="fas fa-receipt"></i>&nbsp; Informasi Pesanan
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="mb-2">
                                <p class="text-muted font-weight-light text-center" style="margin-bottom: 0px">No Invoice Anda</p>
                                <h4 class="font-weight-bold text-center">{{ $transaction->no_invoice }}</h4>
                            </div>

                            <div class="mb-2">
                                <h4 class="font-weight-bold text-center text-muted">
                                    Pesanan anda masih dalam proses menunggu konfirmasi request JTR dari Admin.
                                </h4>
                                <p class="text-muted font-weight-light text-center">
                                    Mohon tunggu atau silahkan hubungi Admin Muslimah Beauty Care.
                                </p>
                            </div>
                        </div>
                    </div>
                @else
                    @if ($transaction->stts_umum != '0')
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-muted">
                                    <i class="fas fa-receipt"></i>&nbsp; Informasi Pesanan
                                    @if ($transaction->is_dropshipper == "Y")
                                        <label class="badge badge-warning">Dropship</label>
                                    @endif
                                </h3>
                            </div>
                            <div class="card-body">
                                <div>
                                    <p class="text-muted font-weight-light text-center" style="margin-bottom: 0px">No Invoice Anda</p>
                                    <h4 class="font-weight-bold text-center">{{ $transaction->no_invoice }}</h4>
                                </div>

                                <div class="mt-2">
                                    <p class="text-muted pb-0 font-weight-light text-center" style="margin-bottom: 0px">Telah dibayar sebesar</p>
                                    <h4 class="font-weight-bold pb-2 text-center">@currency($transaction->ttl_transaksi)</h4>
                                </div>

                                <div class="mt-2">
                                    <p class="text-muted pb-0 font-weight-light text-center" style="margin-bottom: 0px">Status Pesanan</p>
                                    <h4 class="font-weight-bold pb-2 text-center">{{ $transaction->nm_stts_umum }}</h4>
                                </div>

                                <div class="mt-2">
                                    <p class="text-muted pb-0 font-weight-light text-center" style="margin-bottom: 0px">No Resi</p>
                                    @if ($transaction->resi == "")
                                        <h4 class="font-weight-bold pb-2 text-center">-</h4>
                                    @else
                                        <h3 class="text-center">
                                            <label class="badge badge-light" id="resi">{{ $transaction->resi }}</label>
                                            <button class="btn btn-secondary btn-sm" onclick="copyToClipboard('#resi')">
                                                Copy
                                            </button>
                                        </h3>
                                    @endif
                                </div>

                                @if ($transaction->stts_umum == '2')
                                    <button type="button" class="btn btn-pink btn-sm btn-block" data-toggle="modal" data-target="#konfirmasi-pesanan">
                                        KONFIRMASI PESANAN TELAH SAMPAI
                                    </button>
                                @endif
                            </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-muted">
                                    <i class="fas fa-receipt"></i>&nbsp; Informasi Pesanan
                                    @if ($transaction->is_dropshipper == "Y")
                                        <label class="badge badge-warning">Dropship</label>
                                    @endif
                                </h3>
                            </div>
                            <div class="card-body">
                                <div>
                                    <p class="text-muted font-weight-light text-center" style="margin-bottom: 0px">No Invoice Anda</p>
                                    <h4 class="font-weight-bold text-center">{{ $transaction->no_invoice }}</h4>
                                </div>

                                <div class="mt-2">
                                    <p class="text-muted pb-0 font-weight-light text-center" style="margin-bottom: 0px">Tagihan anda sebesar</p>
                                    <h4 class="font-weight-bold pb-2 text-center">@currency($transaction->ttl_transaksi)</h4>
                                </div>

                                <a href="{{ url('/dist/pembayaran', $transaction->no_invoice) }}" class="btn btn-pink btn-sm btn-block">
                                    BAYAR SEKARANG
                                </a>
                            </div>
                        </div>
                    @endif
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-muted"><i class="fas fa-map-marker-alt"></i>&nbsp; Alamat Pengiriman</h3>
                            </div>
                            <div class="card-body">
                                <span class="text-muted">

                                    @if ($transaction->is_dropshipper == "Y")
                                        {{ $dropship->dropship_nama . ' (' . $dropship->dropship_no_hp . ')' }}  <br>
                                        {{ $dropship->dropship_alamat . ', Kec. ' . $dropship->subdistrict_name . ', ' . $dropship->city_name . ', ' . $dropship->province_name }}
                                    @else
                                        {{ $transaction->nama . ' (' . $transaction->no_hp . ')' }}  <br>
                                        {{ $transaction->alamat . ', Kec. ' . $transaction->subdistrict_name . ', ' . $transaction->city_name . ', ' . $transaction->province_name }}
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-muted"><i class="fas fa-truck"></i>&nbsp; Opsi Pengiriman</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="font-weight-medium">{{ $transaction->logistik_name }}</span>
                                    </div>
                                    <div class="col-md-12">
                                        <span class="font-weight-medium">Service {{ $transaction->logistik_service }}</span>
                                    </div>
                                    <div class="col-md-12">
                                        <span class="font-weight-light">{{ $transaction->logistik_day }} hari</span> &nbsp;&nbsp;
                                        <span class="font-weight-bold">@currency($transaction->ttl_ongkir)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-muted"><i class="fas fa-edit"></i>&nbsp; Catatan</h3>
                            </div>
                            <div class="card-body">
                                <span class="text-muted">
                                    {{ $transaction->catatan }}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title text-muted">
                            <i class="fas fa-cubes"></i>&nbsp; Daftar Pesanan
                        </h3>
                    </div>
                    <div class="card-body" style="padding-top: 0px; padding-bottom: 0px">
                        <div class="cart-list">
                            @php
                                $shoppingAmount = 0;
                                $itemAmount = 0;
                                $weightAmount = 0;
                            @endphp
                            @foreach ($myOrder as $item)
                                <div class="border-bottom">
                                    <div class="row my-3">
                                        <div class="col-md-7">
                                            <h5 class="font-weight-normal text-dark">{{ $item->nama }}</h5>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <span class="font-weight-ligt text-muted">{{ $item->qty }} x @currency($item->harga)</span>
                                        </div>
                                        <div class="col-md-2 col-6 text-right">
                                            <span class="font-weight-normal">@currency($item->jumlah)</span>
                                        </div>
                                    </div>
                                </div>
                            @php
                                $shoppingAmount = $shoppingAmount + $item->jumlah;
                                $itemAmount = $itemAmount + $item->qty;
                                $weightAmount = $weightAmount + $item->berat;
                            @endphp
                            @endforeach
                        </div>


                            <div class="row my-3">
                                <div class="col-md-6 col-6">
                                    <span class="text-muted">Berat </span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium">
                                        @php
                                            echo round($transaction->ttl_berat / 1000,2) . " kg";
                                        @endphp
                                    </span>
                                </div>
                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Total Item</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                <span class="font-weight-medium">{{ $itemAmount }} pcs</span>
                                </div>
                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Subtotal untuk Produk</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium">@currency($shoppingAmount)</span>
                                </div>
                                <div class="col-md-6 col-6">
                                    <span class="text-muted font-weight-light">Subtotal Pengiriman</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium cost-subtotal">@currency($transaction->ttl_ongkir)</span>
                                </div>

                                <div class="col-md-6 col-6">
                                    <span class="font-weight-bold">Total Tagihan</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-bold amount">@currency($transaction->ttl_transaksi)</span>
                                </div>
                            </div>



                        <div class="row my-3">
                            @if ($transaction->stts_umum == '2')
                                <button type="button" class="btn btn-pink btn-sm btn-block">
                                    KONFIRMASI PESANAN TELAH SAMPAI
                                </button>
                            @elseif ($transaction->stts_umum == '0')
                                @if ($transaction->request_jtr == 'N' || ($transaction->request_jtr == 'Y' && $transaction->konfirmasi_jtr == 'Y'))
                                    <a href="{{ url('/dist/pembayaran', $transaction->no_invoice) }}" class="btn btn-pink btn-sm btn-block">
                                        BAYAR SEKARANG
                                    </a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('distributor.pesanan.modalKonfirmasiPesananSampai')
<script>
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}
</script>
@endsection
