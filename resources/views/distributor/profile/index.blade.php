@extends('distributor')

@section('content')
<div class="content-header" style="margin-top: 70px;">
    <h4>
        <a href="{{ url('dist/dashboard') }}" style="color: grey">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a>
        &nbsp;&nbsp;
        Profile
    </h4>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="col-md-3">

        <!-- Profile Image -->
        <div class="card card card-outline">
            <div class="card-body box-profile">
                <div class="text-center mb-3">
                    <img class="profile-user-img img-fluid img-circle"
                        src="{{ url('/public/img/user.png') }}"
                        alt="User profile picture">
                </div>

                <h3 class="profile-username text-center font-weight-medium text-dark">{{ $myProfile->nama }}</h3>

                <hr>

                <div class="font-weight-normal text-dark"><i class="fas fa-id-card mr-1"></i> No. Handphone</div>

                <p class="text-muted">
                    {{ $myProfile->no_hp }}
                </p>

                <hr>

                <div class="font-weight-normal text-dark"><i class="fas fa-map-marker-alt mr-1"></i> Alamat</div>

                <p class="text-muted">
                    {{ $myProfile->alamat }}, Kecamatan {{ $myProfile->subdistrict_name }},
                    {{ $myProfile->city_name }}, {{ $myProfile->province_name }}
                </p>

                <a href="{{ url('/dist/profile/form-edit') }}" class="btn btn-pink btn-sm btn-block">
                    <i class="fas fa-edit"></i> Edit Profil
                </a>

                <a href="{{ url('/dist/form-ubah-password') }}" class="btn btn-light btn-sm btn-block">
                    <i class="fas fa-eye"></i> Ubah Password
                </a>
            </div>
        </div>
    </div>
</section>
@endsection
