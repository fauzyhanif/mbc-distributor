@extends('distributor')

@section('content')
<section class="content-header" style="margin-top: 70px;">
    <h4>
        <a href="{{ url('dist/profile') }}" style="color: grey">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a>
        &nbsp;&nbsp;
        Ubah Password
    </h4>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <h5><i class="icon fas fa-check"></i> Berhasil</h5>
                    <p>{{ $message }}</p>
                </div>
                @endif

                @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    Gagal</h5>
                    <p>{{ $message }}</p>
                </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            Form Ubah Password
                        </h3>
                    </div>
                    <form action="{{ url('/dist/ubah-password') }}" method="POST">
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <label>Password Lama</label>
                                <input type="password" class="form-control" name="old_password" placeholder="Password lama">
                            </div>

                            <div class="form-group">
                                <label>Password Baru</label>
                                <input type="password" class="form-control" name="new_password" placeholder="Password baru">
                            </div>

                            <div class="form-group">
                                <label>Konfirmasi Password Baru</label>
                            </div>
                            <input type="password" class="form-control" name="confirm_password" placeholder="Konfirmasi password baru">
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-pink btn-block">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('admin.mbc.distributor.js')
@endsection
