@include('layouts.distributor.header')
<div class="content-wrapper">
    <div class="container">
        @yield('content')

    </div>
</div>
@include('layouts.distributor.footer')
