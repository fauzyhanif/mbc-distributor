@extends('admin')

@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 ml-0 text-dark">Distributor</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Referensi</a></li>
          <li class="breadcrumb-item active">Distributor</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                src="{{ url('/public/img/user.png') }}"
                alt="User profile picture">
          </div>

            <h3 class="profile-username text-center">{{ $data->nama }}</h3>

            <hr>

          <strong><i class="fas fa-id-card mr-1"></i> No. Handphone</strong>

          <p class="text-muted">
            {{ $data->no_hp_1 }} <br>
            {{ $data->no_hp_2 }}
          </p>

          <hr>

          <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>

          <p class="text-muted">
            {{ $data->alamat }}, Kecamatan {{ $data->subdistrict_name }}, {{ $data->city_name }}, {{ $data->province_name }}, {{ $data->kodepos }}
          </p>

        <a href="{{ url('/admin/setting-web/form-edit') }}" class="btn btn-primary btn-block">
            <b>
              <i class="fas fa-edit"></i> Edit Profil
            </b>
          </a>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
  </div>
</section>
@endsection
