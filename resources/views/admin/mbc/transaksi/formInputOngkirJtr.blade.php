<div class="modal" id="modal-input-ongkir-jtr">
    <div class="modal-dialog">

        <form action="{{ url('/admin/transaksi/input-ongkir-jtr', $orderInformation->no_invoice) }}" method="POST">
            @csrf
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Input Ongkir JTR</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="alert alert-warning">
                        <h5>
                            <i class="icon fas fa-info"></i> Mohon perhatian
                        </h5>

                        Setelah admin mengisi form dibawah ini silahkan hubungi distributor guna
                        menginformasikan Jumlah Ongkir dan Total Tagihan yang harus dibayar.
                    </div>

                    <div class="form-group">
                        <label>Silahkan pilih status</label>
                        <select name="konfirmasi_jtr" class="form-control" required>
                            <option value="N" @if ($orderInformation->konfirmasi_jtr == "N") selected @endif>Belum dikonfirmasi</option>
                            <option value="Y" @if ($orderInformation->konfirmasi_jtr == "Y") selected @endif>Sudah dikonfirmasi</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Jasa Pengiriman</label>
                        <input type="text" class="form-control" name="logistik_name"
                            @if ($orderInformation->logistik_name == "")
                                value="JNE"
                            @else
                                value="{{ $orderInformation->logistik_name }}"
                            @endif
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label>Nama Servis</label>
                        <input type="text" class="form-control" name="logistik_service"
                            @if ($orderInformation->logistik_service == "")
                                value="JTR"
                            @else
                                value="{{ $orderInformation->logistik_service }}"
                            @endif
                            required
                        >
                    </div>

                    <div class="form-group">
                        <label>Estimasi Lama Pengiriman <span class="text-muted font-weight-light">(Hari)</span> </label>
                        <input type="text" class="form-control" name="logistik_day" value="{{ $orderInformation->logistik_day }}" required>
                    </div>

                    <div class="form-group">
                        <label>Ongkos Kirim</label>
                        <input type="number" class="form-control" name="ttl_ongkir" value="{{ $orderInformation->ttl_ongkir }}" required>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary pull-left">Simpan Ongkir</button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                </div>

            </div>
        </form>
    </div>
</div>
