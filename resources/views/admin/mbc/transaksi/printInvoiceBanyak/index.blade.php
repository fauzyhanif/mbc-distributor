@extends('admin')
@section('content')

{{-- for url data json --}}
<input type="hidden" name="id_status" value="{{ $idStatus }}">

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-2">
                <h1 class="m-0 ml-0 text-dark">Print Invoice</h1>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <h5><i class="icon fas fa-check"></i> Berhasil</h5>
                    <p>{{ $message }}</p>
                </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            List Data
                        </h3>
                        <div class="card-tools">
                            <select name="" class="form-control" onchange="switchStatus(this.value)">
                                <option value="*">-- Semua Transaksi --</option>
                                @foreach ($status as $x)
                                    <option value="{{ $x->kode }}" @if ($idStatus == $x->kode) selected @endif>{{ $x->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <form action="{{ url('/admin/transaksi/do-print-invoice-banyak') }}" method="POST" target="_blank">
                        @csrf
                        <div class="card-body">
                            <table class="table table-striped" id="data-table">
                                <thead>
                                    <th width="10%">Aksi</th>
                                    <th>No Invoice</th>
                                    <th>Tanggal</th>
                                    <th>Nama</th>
                                    <th>Total Transaksi</th>
                                    <th>Status</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fas fa-print"></i>
                                Print Invoice
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</section>

@include('admin.mbc.transaksi.printInvoiceBanyak.js')
@endsection
