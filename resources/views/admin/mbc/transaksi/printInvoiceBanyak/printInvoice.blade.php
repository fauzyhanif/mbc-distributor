<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Invoice Print | MBC Family</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap 4 -->

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('public/v4/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('public/v4/dist/css/adminlte.min.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <style>
        .invoice {
            width: 45%;
            border-bottom : 1px dashed black;
            margin-bottom: 30px;
        }

        .logo {
            width: 50px
        }

        .logistic {
            margin-bottom: 30px
        }

        .logistic-name {
            margin-top: -18px;
        }
    </style>
</head>
<body>
<div class="wrapper">
    @foreach ($orders as $order)
        <section class="invoice">
            <div class="row mb-1 header">
                <div class="col-12">
                    <h2 class="page-header">
                    <img src="{{ url('/public/img/logo_mbc_family_v1.jpg') }}" alt="logo" class="logo">
                        Muslimah Beauty Care.
                    </h2>
                </div>
            </div>

            <div class="row invoice-info">
                <div class="col-sm-12 col-12 invoice-col">
                    <b>No Invoice : </b> {{ $order->no_invoice }}
                </div>

                <div class="col-sm-12 col-12 invoice-col">
                    <address>
                        @if($order->is_dropshipper == "N")
                            <strong>
                                {{ $order->nama . ' (' . $order->no_hp . ')' }}
                            </strong>

                            <br>

                            {{
                                $order->alamat .
                                ', Kec. ' . $order->gnrl_subdistrict .
                                ', ' . $order->gnrl_city .
                                ', ' . $order->gnrl_province
                            }}
                        @else
                            <strong>
                                {{ $order->dropship_nama . ' (' . $order->dropship_no_hp . ')' }}
                            </strong>

                            <br>

                            {{
                                $order->dropship_alamat .
                                ', Kec. ' . $order->drp_subdistrict .
                                ', ' . $order->drp_city .
                                ', ' . $order->drp_province
                            }}
                        @endif
                    </address>
                </div>
            </div>

            <div class="row order-list">
                <div class="col-12 table-responsive">
                    <table width="100%" border="1">
                        <thead>
                            <th width="10%"> &nbsp; &nbsp; No</th>
                            <th> &nbsp; &nbsp; Produk</th>
                            <th> &nbsp; &nbsp; Qty</th>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($order->order_list as $item)
                                <tr>
                                    <td>&nbsp; &nbsp; {{ $no++ }}.</td>
                                    <td>&nbsp; &nbsp; {{ $item->nama }}</td>
                                    <td>&nbsp; &nbsp; {{ $item->qty }} pcs</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row logistic">
                <div class="col-md-12 col-12">
                    <p>Pengiriman:</p>
                    <p class="well well-sm shadow-none font-weight-bold logistic-name">
                        {{ $order->logistik_name }}
                        {{ $order->logistik_service }}
                    </p>
                </div>
            </div>
        </section>
    @endforeach
</div>

<script>
  window.addEventListener("load", window.print());
</script>
</body>
</html>
