<script>
$(function() {
    var status = $('input[name="id_status"]').val();
    var base = {!! json_encode(url('/admin/transaksi/get-json-for-print')) !!};

    $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: base + '/' + status,
        columns: [
            { data: 'btn_action', name: 'btn_action', searchable: false, className: 'text-center' },
            { data: 'no_invoice', name: 'a.no_invoice', searchable: true },
            { data: 'tgl_transaksi', name: 'tgl_transaksi', searchable: false },
            { data: 'nama_edited', name: 'nama_edited', searchable: true },
            { data: 'ttl_transaksi', name: 'ttl_transaksi', searchable: false },
            { data: 'badge_status', name: 'badge_status', searchable: false },
        ]
    });

    $('[data-toggle="tooltip"]').tooltip()
});

function inputResi(status) {
    if (status == "2") {
        $('.form-resi').css("display", "block");
    } else {
        $('.form-resi').css("display", "none");
    }
}

function switchStatus(id) {
    var base = {!! json_encode(url('/admin/transaksi/print-invoice-banyak')) !!};
    var urlReload = base + "/" + id;

    //this will redirect us in same window
    document.location.href = urlReload;
}
</script>
