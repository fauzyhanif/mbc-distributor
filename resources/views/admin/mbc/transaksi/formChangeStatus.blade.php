<div class="modal" id="modal-change-status-trx">
    <div class="modal-dialog">

        <form action="{{ url('/admin/transaksi/ubah-status', $orderInformation->no_invoice) }}" method="POST">
            @csrf
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Ubah Status Transaksi</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label>Silahkan pilih status</label>
                        <select name="stts_umum" class="form-control" onchange="inputResi(this.value)">
                            @foreach ($sttsTransaksi as $status)
                                <option value="{{ $status->kode }}" {{ $orderInformation->stts_umum == $status->kode ? 'selected' : '' }}>{{ $status->nama }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group form-resi" @if($orderInformation->stts_umum != "2" && $orderInformation->stts_umum != "3") style="display: none" @endif>
                        <label>Nomor Resi</label>
                        <input type="text" class="form-control" name="resi">
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary pull-left">Ubah Status</button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                </div>

            </div>
        </form>
    </div>
</div>
