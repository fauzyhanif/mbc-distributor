@extends('admin')
@section('content')

{{-- for url data json --}}
<input type="hidden" name="id_status" value="{{ $idStatus }}">

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-2">
                <h1 class="m-0 ml-0 text-dark">Transaksi</h1>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>{{ $total->total }}</h3>

                        <p>Total Transaksi</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-6">
              <!-- small box -->
                <div class="small-box bg-secondary">
                    <div class="inner">
                        <h3>{{ $total->belum_bayar }}</sup></h3>

                        <p>Belum Bayar</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-spinner"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>{{ $total->dikemas }}</sup></h3>

                        <p>Dikemas</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-cube"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-6">
            <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ $total->dikirim }}</sup></h3>

                        <p>Dikirim</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-truck"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-6">
            <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{ $total->selesai }}</sup></h3>

                        <p>Selesai</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-check-circle"></i>
                    </div>
                </div>
            </div>

          </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <h5><i class="icon fas fa-check"></i> Berhasil</h5>
                    <p>{{ $message }}</p>
                </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            List Data
                        </h3>
                        <div class="card-tools">
                            <select name="" class="form-control" onchange="switchStatus(this.value)">
                                <option value="*">-- Semua Transaksi --</option>
                                @foreach ($status as $x)
                                    <option value="{{ $x->kode }}" @if ($idStatus == $x->kode) selected @endif>{{ $x->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped" id="data-table">
                            <thead>
                                <th>No Invoice </th>
                                <th>Tanggal</th>
                                <th>Nama</th>
                                <th>Total Transaksi</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</section>

@include('admin.mbc.transaksi.js')
@endsection
