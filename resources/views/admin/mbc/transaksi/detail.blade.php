@extends('admin')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ url('/admin/list-transaksi') }}" class="btn btn-default">
                    <i class="fas fa-long-arrow-alt-left"></i> Kembali
                </a>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <h5><i class="icon fas fa-check"></i> Berhasil</h5>
                    <p>{{ $message }}</p>
                </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="fas fa-map-marker-alt"></i>&nbsp; Informasi Order
                                </h3>
                            </div>
                            <div class="card-body">
                                @if ($orderInformation->is_dropshipper == "Y")
                                    <label class="badge badge-warning">Dropship</label>
                                @endif

                                @if ($orderInformation->request_jtr == "Y")
                                    @if ($orderInformation->konfirmasi_jtr == "Y")
                                        <label class="badge badge-info">Jtr berhasil dikonfirmasi</label>
                                    @else
                                        <label class="badge badge-info">Request Jtr</label>
                                    @endif

                                    <a href="#" style="text-decoration: underline" data-toggle="modal" data-target="#modal-input-ongkir-jtr">Input Ongkir JTR disini</a>
                                @endif

                                @if ($orderInformation->request_jtr == "Y" || $orderInformation->is_dropshipper == "Y")
                                <br>
                                @endif

                                <span class="text-muted">No Invoice</span>
                                <p class="font-weight-medium">
                                    {{ $orderInformation->no_invoice }}
                                </p>

                                <hr>

                                <span class="text-muted">Status Transaksis</span>
                                <h5 class="font-weight-medium">
                                    @php
                                        $type = "";

                                        if ($orderInformation->stts_umum == '0') {
                                            $type = "secondary";
                                        } elseif ($orderInformation->stts_umum == '1') {
                                            $type = "warning";
                                        } elseif ($orderInformation->stts_umum == '2') {
                                            $type = "info";
                                        } elseif ($orderInformation->stts_umum == '3') {
                                            $type = "success";
                                        }

                                        echo "<span class='badge badge-$type'>$orderInformation->nm_status</span>";
                                    @endphp
                                </h5>

                                <hr>

                                <span class="text-muted">No Resi</span>
                                <p class="font-weight-medium">
                                    {{ $orderInformation->resi == "" ? "-" : $orderInformation->resi }}
                                </p>

                                <span class="text-muted">Customer</span>
                                <p class="font-weight-medium">
                                    {{ $orderInformation->nama . " (" . $orderInformation->city_name . ")" }}
                                </p>

                                <hr>

                                <span class="text-muted">Alamat Pengiriman</span>
                                <p class="font-weight-medium">
                                    @if ($orderInformation->is_dropshipper == "Y")
                                        {{ $dropship->dropship_nama . ' (' . $dropship->dropship_no_hp . ')' }}  <br>
                                        {{ $dropship->dropship_alamat . ', Kec. ' . $dropship->subdistrict_name . ', ' . $dropship->city_name . ', ' . $dropship->province_name }}
                                    @else
                                        {{ $orderInformation->nama . ' (' . $orderInformation->no_hp . ')' }}  <br>
                                        {{ $orderInformation->alamat . ', Kec. ' . $orderInformation->subdistrict_name . ', ' . $orderInformation->city_name . ', ' . $orderInformation->province_name }}
                                    @endif
                                </p>

                                <hr>

                                <span class="text-muted">Logistik Pengiriman</span>
                                <p class="font-weight-medium">
                                    {{ $orderInformation->logistik_name }}  <br>
                                    Service {{ $orderInformation->logistik_service }} <br>
                                    {{ $orderInformation->logistik_day }} hari &nbsp;&nbsp;  @currency($orderInformation->ttl_ongkir)
                                </p>

                                <hr>

                                <span class="text-muted">Catatan Pesanan</span>
                                <p class="font-weight-medium">
                                    {{ $orderInformation->catatan }}
                                </p>

                                <hr>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-cubes"></i>&nbsp; Daftar Pesanan
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="cart-list">
                            @php
                                $shoppingAmount = 0;
                                $itemAmount = 0;
                                $weightAmount = 0;
                            @endphp
                            @foreach ($orderList as $item)
                                <div class="border-bottom">
                                    <div class="row my-3">
                                        <div class="col-md-7">
                                            <h5 class="font-weight-bold">{{ $item->nama }}</h5>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <span class="font-weight-medium text-muted">{{ $item->qty }} x @currency($item->harga)</span>
                                        </div>
                                        <div class="col-md-2 col-6 text-right">
                                            <span class="font-weight-bold">@currency($item->jumlah)</span>
                                        </div>
                                    </div>
                                </div>
                            @php
                                $shoppingAmount = $shoppingAmount + $item->jumlah;
                                $itemAmount = $itemAmount + $item->qty;
                                $weightAmount = $weightAmount + ($item->qty * $item->berat );
                            @endphp
                            @endforeach
                        </div>


                            <div class="row my-3">
                                <div class="col-md-6 col-6">
                                    <span class="text-muted">Berat </span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium">
                                        @php
                                            echo round($orderInformation->ttl_berat / 1000, 2) . " kg";
                                        @endphp
                                    </span>
                                </div>
                                <div class="col-md-6 col-6">
                                    <span class="text-muted">Total Item</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                <span class="font-weight-medium">{{ $itemAmount }} pcs</span>
                                </div>
                                <div class="col-md-6 col-6">
                                    <span class="text-muted">Subtotal untuk Produk</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium">@currency($shoppingAmount)</span>
                                </div>
                                <div class="col-md-6 col-6">
                                    <span class="text-muted">Subtotal Pengiriman</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-medium cost-subtotal">@currency($orderInformation->ttl_ongkir)</span>
                                </div>

                                <div class="col-md-6 col-6">
                                    <span class="font-weight-bold">Total Tagihan</span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span class="font-weight-bold amount">@currency($orderInformation->ttl_transaksi)</span>
                                </div>
                            </div>



                        <div class="row my-3">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#modal-change-status-trx">
                                    Ubah Status Transaksi
                                </button>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ url('/admin/transaksi/print-invoice', $orderInformation->no_invoice) }}" target="_blank" class="btn btn-default btn-sm btn-block">
                                    Print Invoice
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('admin.mbc.transaksi.formChangeStatus')
@include('admin.mbc.transaksi.formInputOngkirJtr')
@include('admin.mbc.transaksi.js')
@endsection
