<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Invoice Print | MBC Family</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap 4 -->

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('public/v4/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('public/v4/dist/css/adminlte.min.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <style>
        .invoice {
            width: 45%;
            border-bottom : 1px dashed black;
            margin-bottom: 30px;
        }

        .logo {
            width: 50px
        }

        .logistic {
            margin-bottom: 30px
        }

        .logistic-name {
            margin-top: -18px;
        }
    </style>
</head>
<body>
<div class="wrapper">

    <section class="invoice">
        @if($info->is_dropshipper == "N")
            <div class="row mb-1">
                <div class="col-12">
                    <h2 class="page-header">
                    <img src="{{ url('/public/img/logo_mbc_family_v1.jpg') }}" alt="logo" class="logo">
                        Muslimah Beauty Care.
                    </h2>
                </div>
            </div>
        @endif

        <div class="row invoice-info">
            <div class="col-sm-12 col-12 invoice-col">
                <b>No Invoice : </b> {{ $info->no_invoice }}
            </div>

            <div class="col-sm-12 col-12 invoice-col">
                <address>
                    @if($info->is_dropshipper == "N")
                        <strong>
                            {{ $info->nama . ' (' . $info->no_hp . ')' }}
                        </strong>
                        <br>
                        {{
                            $info->alamat .
                            ', Kec. ' . $info->subdistrict_name .
                            ', ' . $info->city_name .
                            ', ' . $info->province_name
                        }}
                    @else
                        <strong>
                            {{ $dropship->dropship_nama . ' (' . $dropship->dropship_no_hp . ')' }}
                        </strong>
                        <br>
                        {{
                            $dropship->dropship_alamat .
                            ', Kec. ' . $dropship->subdistrict_name .
                            ', ' . $dropship->city_name .
                            ', ' . $dropship->province_name
                        }}
                    @endif
                </address>
            </div>
        </div>

        <div class="row">
            <div class="col-12 table-responsive">
                <table width="100%" border="1">
                    <thead>
                        <th width="10%"> &nbsp; &nbsp; No</th>
                        <th> &nbsp; &nbsp; Produk</th>
                        <th> &nbsp; &nbsp; Qty</th>
                    </thead>
                    <tbody>
                        @php $no = 1; @endphp
                        @foreach ($orderList as $item)
                            <tr>
                                <td>&nbsp; &nbsp; {{ $no++ }}.</td>
                                <td>&nbsp; &nbsp; {{ $item->nama }}</td>
                                <td>&nbsp; &nbsp; {{ $item->qty }} pcs</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row logistic">
            <div class="col-md-12 col-12">
                <p>Pengiriman:</p>
                <p class="well well-sm shadow-none font-weight-bold logistic-name">
                    {{ $info->logistik_name }}
                    {{ $info->logistik_service }}
                </p>
                </div>
        </div>
    </section>
</div>

<script>
  window.addEventListener("load", window.print());
</script>
</body>
</html>
