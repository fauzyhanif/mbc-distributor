@extends('admin')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 ml-0 text-dark">Distributor</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item active">Distributor</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info">
                Jika distributor lupa password silahkan buka menu <a href="{{ url('/user') }}">user</a> dan klik data distributor yang ingin diubah passwordnya kemudian isi form password
                    dengan password yang diinginkan.
                </div>
            </div>

            <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <h5><i class="icon fas fa-check"></i> Berhasil</h5>
                    <p>{{ $message }}</p>
                </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            List Data
                        </h3>
                        <div class="card-tools">
                            <a href="{{ url('/admin/distributor/form-add') }}" class="btn btn-success btn-sm pull-right">
                                <i class="fa fa-plus"></i> Tambah Distributor
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <th>No</th>
                                <th>Nama </th>
                                <th>Kota</th>
                                <th>No. Handphone</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp

                                @foreach ($datas as $item)
                                <tr>
                                    <td>{{ $no++ }}.</td>
                                    <td>{{ $item->nama }}</td>
                                    <td>{{ $item->city_name }}</td>
                                    <td>{{ $item->no_hp }}</td>
                                    <td>
                                        @if ($item->is_aktif == 'Y')
                                            <label class="badge badge-success">Aktif</label>
                                        @else
                                            <label class="badge badge-danger">Non Aktif</label>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('/admin/distributor/form-edit', $item->id) }}" class="btn btn-primary btn-xs float-left mr-1">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                        <form action="{{ url('/admin/distributor/aktifasi', $item->id) }}" method="POST">
                                            @csrf
                                            @if ($item->is_aktif == 'Y')
                                                <input type="hidden" name="is_aktif" value="N">
                                                <button type="submit" class="btn btn-danger btn-xs float-left">
                                                    <i class="fas fa-power-off"></i> Nonaktifkan
                                                </button>
                                            @else
                                                <input type="hidden" name="is_aktif" value="Y">
                                                <button type="submit" class="btn btn-success btn-xs float-left">
                                                    <i class="fas fa-power-off"></i> Aktifkan
                                                </button>
                                            @endif
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection
