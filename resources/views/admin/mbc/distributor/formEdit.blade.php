@extends('admin')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ url('/admin/distributor') }}" class="btn btn-default btn-sm">
                    <i class="fas fa-long-arrow-alt-left"></i> Kembali
                </a>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item active">Distributor</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            Form Edit Data
                        </h3>
                    </div>
                    <form action="{{ url('/admin/distributor/edit', $data->id) }}" method="POST">
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="nama" value="{{ $data->nama }}" placeholder="Nama">
                            </div>

                            <div class="form-group">
                                <label>No Handphone</label>
                                <input type="text" class="form-control" name="no_hp" value="{{ $data->no_hp }}" placeholder="No Handphone">
                            </div>

                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea name="alamat" class="form-control">{{ $data->alamat }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Provinsi</label>
                                <select name="id_provinsi" class="form-control" required onchange="cariKota(this.value)">
                                    <option value="">-- Pilih Provinsi --</option>
                                    @foreach ($dtProvinsi as $item)
                                    <option value="{{ $item->province_id }}" {{ $data->id_provinsi == $item->province_id ? 'selected' : '' }}>{{ $item->province_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Kota</label>
                                <select name="id_kota" class="form-control" id="kota" required onchange="cariKecamatan(this.value)">
                                    <option value="">-- Pilih Kota / Kabupaten --</option>
                                    @foreach ($dtKota as $item)
                                    <option value="{{ $item->city_id }}" {{ $data->id_kota == $item->city_id ? 'selected' : '' }}>{{ $item->city_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Kecamatan</label>
                                <select name="id_kecamatan" class="form-control" id="kecamatan" required>
                                    <option value="">-- Pilih Kecamatan --</option>
                                    @foreach ($dtKecamatan as $item)
                                    <option value="{{ $item->subdistrict_id }}" {{ $data->id_kecamatan == $item->subdistrict_id ? 'selected' : '' }}>{{ $item->subdistrict_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('admin.mbc.distributor.js')
@endsection
