@extends('admin')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ url('/admin/produk') }}" class="btn btn-default btn-sm">
                    <i class="fas fa-long-arrow-alt-left"></i> Kembali
                </a>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item active">Produk</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            Form Tambah Data
                        </h3>
                    </div>
                    <form action="{{ url('/admin/produk/add-new') }}" enctype="multipart/form-data" method="POST">
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <label>Kode Produk <span class="text-red">*</span></label>
                                <input type="text" class="form-control" name="kode" value="{{ old('kode') }}" required placeholder="Kode Produk">
                            </div>

                            <div class="form-group">
                                <label>Nama Produk <span class="text-red">*</span></label>
                                <input type="text" class="form-control" name="nama" value="{{ old('nama') }}" required placeholder="Nama Produk">
                            </div>

                            <div class="form-group">
                                <label>Deskripsi Produk</label>
                                <textarea name="deskripsi" class="form-control">{{ old('deskripsi') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Kategori Produk <span class="text-red">*</span></label>
                                <select name="kategori_produk" class="form-control" required>
                                    <option value="">-- Pilih Kategori Produk --</option>
                                    @foreach ($dtKategoriProduk as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Thumbnail <span class="text-red">*</span> <span class="font-weight-light">Ukuran file maksimal <b>1mb</b> dan file berformat <b>JPG, JPEG, PNG</b></span></label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="thumbnail" class="custom-file-input form-file" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Berat Produk <span class="text-red">*</span> <span class="font-weight-light">(Hitungan Gram)</span></label>
                                <input type="number" class="form-control" name="berat" value="{{ old('berat') }}" required placeholder="Berat Produk">
                            </div>

                            <div class="form-group">
                                <label>Stok</label>
                                <input type="number" class="form-control" name="stok" value="{{ old('stok') }}" placeholder="Stok">
                            </div>

                            <div class="form-group">
                                <label>Harga Modal <span class="text-red">*</span></label>
                                <input type="number" class="form-control" name="harga_modal" value="{{ old('harga_modal') }}" required placeholder="Harga Modal">
                            </div>

                            <div class="form-group">
                                <label>Harga Jual <span class="text-red">*</span></label>
                                <input type="number" class="form-control" name="harga_jual" value="{{ old('harga_jual') }}" required placeholder="Harga Jual">
                            </div>

                            <div class="callout callout-info">
                                <h4>Perhatian</h4>
                                <p>Ukuran file maksimal <b>1mb</b> dan file berformat <b>JPG, JPEG, PNG</b></p>
                            </div>

                            <div class="form-group">
                                <label>Foto Pendukung 1</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="nama_file[]" class="custom-file-input form-file" id="exampleInputFile" multiple>
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Foto Pendukung 2</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="nama_file[]" class="custom-file-input form-file" id="exampleInputFile" multiple>
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Foto Pendukung 3</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="nama_file[]" class="custom-file-input form-file" id="exampleInputFile" multiple>
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Foto Pendukung 4</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="nama_file[]" class="custom-file-input form-file" id="exampleInputFile" multiple>
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Foto Pendukung 5</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="nama_file[]" class="custom-file-input form-file" id="exampleInputFile" multiple>
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(function () {
      bsCustomFileInput.init();
    });

    function clone(params) {
        $(".wrap-foto-pendukung:last").clone().insertAfter("div.result-clone:last");
    }

    // if file selected
    $('.form-file').change(function() {
        var filePath = this.value;

        // Allowing file type
        var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;

        if (!allowedExtensions.exec(filePath)) {
            alert('Invalid file type');
            this.value = '';
            return false;
        } else {
            var FileSize = this.files[0].size / 1024 / 1024; // in MB
            if (FileSize > 1) {
                alert("Mohon maaf file terlalu besar, maximal file 1 mb.");
                this.value = '';
                return false;
            }
        }
    });
</script>
@endsection
