@extends('admin')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 ml-0 text-dark">Laporan Laba per Produk</h1>
                <h5 class="text-muted">Data tanggal : {{ HelperDataReferensi::konversiTgl($firstDay, 'T') }} s/d {{ HelperDataReferensi::konversiTgl($lastDay, 'T') }}</h5>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header with-border">
                        <form action="" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" name="tgl_transaksi" class="form-control" id="reservation">
                                </div>
                                <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped" id="data-table">
                            <thead>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Stok</th>
                                <th>Terjual</th>
                                <th>Jumlah Laba</th>
                            </thead>
                            <tbody>
                                @php
                                    $ttl_stok = 0;
                                    $ttl_terjual = 0;
                                    $ttl_keuntungan = 0;
                                @endphp
                                @foreach ($datas as $data)
                                    <tr>
                                        <td>{{ $data->kode }}</td>
                                        <td>{{ $data->nama }}</td>
                                        <td>
                                            @php
                                                echo $arrProducts[$data->kode]
                                            @endphp
                                        </td>
                                        <td>{{ $data->terjual }} pcs</td>
                                        <td>@currency($data->keuntungan)</td>
                                    </tr>
                                @php
                                    $ttl_stok = $ttl_stok + $data->stok;
                                    $ttl_terjual = $ttl_terjual + $data->terjual;
                                    $ttl_keuntungan = $ttl_keuntungan + $data->keuntungan;
                                @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2"><b>Total</b></td>
                                    <td>{{ $ttl_stok }} pcs</td>
                                    <td>{{ $ttl_terjual }} pcs</td>
                                    <td>@currency($ttl_keuntungan)</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</section>

<script>
$(document).ready(function () {
    var currentTime = new Date();
    // First Date Of the month
    var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
    // Last Date Of the Month
    var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
    $('#reservation').daterangepicker({
        startDate: startDateFrom,
        endDate: startDateTo,
        locale : {
            format : 'YYYY-MM-DD'
        }
    })
});
</script>
@endsection
