@extends('admin')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 ml-0 text-dark">Laporan Omset Distributor</h1>
                <h5 class="text-muted">Data tanggal : {{ HelperDataReferensi::konversiTgl($firstDay, 'T') }} s/d {{ HelperDataReferensi::konversiTgl($lastDay, 'T') }}</h5>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header with-border">
                        <form action="" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" name="tgl_transaksi" class="form-control" id="reservation">
                                </div>
                                <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped" id="data-table">
                            <thead>
                                <th>No.</th>
                                <th>Nama Distributor</th>
                                <th>Asal</th>
                                <th>Qty</th>
                                <th>Jumlah Omset</th>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                    $ttl_qty = 0;
                                    $ttl_omset = 0;
                                @endphp
                                @foreach ($datas as $data)
                                    <tr>
                                        <td>{{ $no }}.</td>
                                        <td>{{ $data->nama }}</td>
                                        <td>{{ $data->city_name }}</td>
                                        <td>{{ $data->jml_qty }} pcs</td>
                                        <td>@currency($data->jml_omset)</td>
                                    </tr>
                                @php
                                    $no = $no + 1;
                                    $ttl_qty = $ttl_qty + $data->jml_qty;
                                    $ttl_omset = $ttl_omset + $data->jml_omset;
                                @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3"><b>Total</b></td>
                                    <td>{{ $ttl_qty }} pcs</td>
                                    <td>@currency($ttl_omset)</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="card-footer">
                        <span class="text-blue font-italic">
                            Note : <br>
                            1. <b>Jumlah Omset</b> tidak termasuk biaya ongkir. <br>
                            2. Data diurutkan berdasarkan <b>Jumlah Omset tertinggi</b>.
                        </span>
                    </div>
                </div>
            </div>
        </div>
</section>

<script>
$(document).ready(function () {
    var currentTime = new Date();
    // First Date Of the month
    var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
    // Last Date Of the Month
    var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
    $('#reservation').daterangepicker({
        startDate: startDateFrom,
        endDate: startDateTo,
        locale : {
            format : 'YYYY-MM-DD'
        }
    })
});
</script>
@endsection
