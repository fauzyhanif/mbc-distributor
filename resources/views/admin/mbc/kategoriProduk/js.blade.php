<script>
    function cariKota(IdProvinsi) {
    var base = {!! json_encode(url('/helpers/cari-kota')) !!};
    var url = base + "/" + IdProvinsi;
    $.ajax({
        type: "GET",
        url: url,
        dataType : 'json',
        success: function(result){
            var y = '<option value="">-- Pili Kota/Kabupaten --</option>'
            for (var x in result){
                y += '<option value="'+result[x].city_id+'">'+result[x].city_name+'</option>'
            }

            $('#kota').html(y)

        }
    });
}

function cariKecamatan(IdKota) {
    var base = {!! json_encode(url('/helpers/cari-kecamatan')) !!};
    var url = base + "/" + IdKota;
    $.ajax({
        type: "GET",
        url: url,
        dataType : 'json',
        success: function(result){
            var y = '<option value="">-- Pilih Kecamatan --</option>'
            for (var x in result){
                y += '<option value="'+result[x].subdistrict_id+'">'+result[x].subdistrict_name+'</option>'
            }

            $('#kecamatan').html(y)

        }
    });
}
</script>
