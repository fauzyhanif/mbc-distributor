@extends('admin')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ url('/usergroup') }}" class="btn btn-default btn-sm">
                    <i class="fas fa-long-arrow-alt-left"></i> Kembali
                </a>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item active">Usergroup</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            Form Tambah Data
                        </h3>
                    </div>
                    <form action="{{ url('/usergroup/add-new') }}" method="POST">
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <label>ID Usergroup</label>
                                <input type="number" class="form-control" name="id_usergroup" value="{{ old('id_usergroup') }}" placeholder="ID Usergroup">
                            </div>

                            <div class="form-group">
                                <label>Usergroup</label>
                                <input type="text" class="form-control" name="nama" value="{{ old('nama') }}" placeholder="Usergroup">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
