@extends('admin')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 ml-0 text-dark">User</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item active">User</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info">
                    Jika ada distributor / admin lupa password siahkan klik edit kemudian isi form password
                    dengan password yang diinginkan.
                </div>
            </div>

            <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            List Data
                        </h3>
                        <div class="card-tools">
                            <a href="{{ url('/user/form-add') }}" class="btn btn-primary btn-sm pull-right">
                                <i class="fa fa-plus"></i> Tambah User
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped" id="users-table">
                            <thead>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Usergroup</th>
                                <th width="30%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'user-json',
        columns: [
            { data: 'nama', name: 'nama' },
            { data: 'username', name: 'username' },
            { data: 'jenis', name: 'jenis' },
            { data: 'aksi', name: 'aksi' }
        ]
    });

});
</script>

@endsection
