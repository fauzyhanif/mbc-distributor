@extends('admin')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ url('/user') }}" class="btn btn-default btn-sm">
                    <i class="fas fa-long-arrow-alt-left"></i> Kembali
                </a>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item active">User</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="card">
                    <div class="card-header with-border">
                        <h3 class="card-title">
                            Form Tambah Data
                        </h3>
                    </div>
                    <form action="{{ url('/user/add-new') }}" method="POST">
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" name="nama" value="{{ old('nama') }}" class="form-control" placeholder="Nama Lengkap">
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="username" value="{{ old('username') }}" class="form-control" placeholder="Username">
                                <span class="text-muted font-italic">username tidak boleh ada spasi</span>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="text" name="password" value="{{ old('password') }}" class="form-control" placeholder="Password">
                                <span class="text-muted font-italic">password minimal 6 karakter dan tidak boleh ada spasi</span>
                            </div>
                            <div class="form-group">
                                <label>Usergroup</label>
                                <select name="id_usergroup[]" class="form-control select2" multiple="multiple" data-placeholder="Pilih usergroup" style="width: 100%;">
                                    @foreach ($dt_usergroup as $x)
                                        <option value="{{ $x->id_usergroup }}">{{ $x->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function() {
    $('.select2').select2({
        closeOnSelect: true
    });
});
</script>
@endsection
