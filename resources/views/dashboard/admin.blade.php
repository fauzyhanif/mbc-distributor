@extends('admin')

@section('content')
<section class="content-header">
    <h1>Dashboard</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>@currency(HelperDashboard::getTotalTransaksi())</h3>

                        <p>Total Transaksi</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-shopping-cart"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-1">
                <!-- small box -->
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3>@currency(HelperDashboard::getTotalTransaksiThisMonth())</h3>

                        <p>Transaksi Bulan Ini</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-shopping-cart"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-1">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>@currency(HelperDashboard::getTotalTransaksiToday())</h3>

                        <p>Transaksi Hari Ini</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-shopping-cart"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-12">
              <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{ HelperDashboard::getTotalDistributor() }}</h3>

                        <p>Distributor</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-users"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>{{ HelperDashboard::getTotalKategoriProduk() }}</h3>

                        <p>Kategori Produk</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-project-diagram"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-12">
            <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ HelperDashboard::getTotalProduk() }}</h3>

                        <p>Produk</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-cubes"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            5 Distributor dengan transaksi tertinggi
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Transaksi</th>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach (HelperDashboard::getOmsetDistributor() as $item)
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>
                                            <p>
                                                {{ $item->nama }} <br>
                                                <span class="text-muted">{{ $item->city_name }}</span>
                                            </p>
                                        </td>
                                        <td>@currency($item->jml_omset)</td>
                                    </tr>
                                @php
                                    $no = $no + 1;
                                @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            5 Transaksi terakhir
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <th>No Invoice</th>
                                <th>Distributor</th>
                                <th>Status</th>
                                <th>Jumlah</th>
                            </thead>
                            <tbody>
                                @foreach (HelperDashboard::getLastTrx() as $item)
                                <tr>
                                    <td>{{ $item->no_invoice }}</td>
                                    <td>{{ $item->nama }}</td>
                                    <td>{{ $item->status }}</td>
                                    <td>@currency($item->ttl_transaksi)</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
