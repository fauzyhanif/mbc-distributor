<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
///////////////////////////////////////////
//////////////* ADMIN *//////////////
///////////////////////////////////////////

# home page
Route::get('/', 'DashboardController@index');
Route::get('', 'DashboardController@index');

# Dashboard
Route::get('/dashboard', 'DashboardController@index')->middleware('admin');

# Setting Web
Route::get('/admin/setting-web', 'SettingWebController@index')->middleware('admin');
Route::get('/admin/setting-web/form-edit', 'SettingWebController@formEdit')->middleware('admin');
Route::post('/admin/setting-web/edit/{id}', 'SettingWebController@edit')->middleware('admin');

# Helper
Route::get('/helpers/cari-kota/{id}', 'HelpersController@cariKota');
Route::get('/helpers/cari-kecamatan/{id}', 'HelpersController@cariKecamatan');
Route::get('/helpers/cari-desa/{id}', 'HelpersController@cariDesa');

# Auth page
Route::get('/auth', 'AuthController@index');
Route::post('/doAuth', 'AuthController@doAuth');
Route::get('/auth/pilih-usergroup', 'AuthController@pilihUsergroup');
Route::get('/set-usergroup/{id}', 'AuthController@setUsergroup');
Route::get('/logout', 'AuthController@logout');
Route::get('/access-denied', 'AuthController@accessDenied');

# Pilih Sesi
Route::get('/pilih-sesi', 'SessionController@pilihSesiTahunAkd');
Route::get('/set-sesi-tahun-akd/{thnakd}/{gelombang}', 'SessionController@setSesiTahunAkd');

# User page
Route::get('/user', 'UserController@index')->middleware('admin');
Route::get('/user-json', 'UserController@json')->middleware('admin');
Route::get('/user/form-add', 'UserController@formAdd')->middleware('admin');
Route::post('/user/add-new', 'UserController@addNew')->middleware('admin');
Route::get('/user/form-edit/{id}', 'UserController@formEdit')->middleware('admin');
Route::post('/user/edit/{id}', 'UserController@edit')->middleware('admin');
Route::get('/user/delete/{id}', 'UserController@delete')->middleware('admin');
Route::get('/user/reset-password/{id}', 'UserController@resetPassword')->middleware('admin');

# Usergroup page
Route::get('/usergroup', 'UsergroupController@index')->middleware('admin');
Route::get('/usergroup/form-add', 'UsergroupController@formAdd')->middleware('admin');
Route::post('/usergroup/add-new', 'UsergroupController@addNew')->middleware('admin');
Route::get('/usergroup/form-edit/{id}', 'UsergroupController@formEdit')->middleware('admin');
Route::post('/usergroup/edit/{id}', 'UsergroupController@edit')->middleware('admin');
Route::get('/usergroup/delete/{id}', 'UsergroupController@delete')->middleware('admin');

# CRUD Distributor
Route::get('/admin/distributor', 'DistributorController@index')->middleware('admin');
Route::get('/admin/distributor/form-add', 'DistributorController@formAdd')->middleware('admin');
Route::post('/admin/distributor/add-new', 'DistributorController@addNew')->middleware('admin');
Route::get('/admin/distributor/form-edit/{id}', 'DistributorController@formEdit')->middleware('admin');
Route::post('/admin/distributor/edit/{id}', 'DistributorController@edit')->middleware('admin');
Route::post('/admin/distributor/delete/{id}', 'DistributorController@delete')->middleware('admin');
Route::post('/admin/distributor/aktifasi/{id}', 'DistributorController@aktifasi')->middleware('admin');

# CRUD Kategori Produk
Route::get('/admin/kategori-produk', 'KategoriProdukController@index')->middleware('admin');
Route::get('/admin/kategori-produk/form-add', 'KategoriProdukController@formAdd')->middleware('admin');
Route::post('/admin/kategori-produk/add-new', 'KategoriProdukController@addNew')->middleware('admin');
Route::get('/admin/kategori-produk/form-edit/{id}', 'KategoriProdukController@formEdit')->middleware('admin');
Route::post('/admin/kategori-produk/edit/{id}', 'KategoriProdukController@edit')->middleware('admin');
Route::post('/admin/kategori-produk/delete/{id}', 'KategoriProdukController@delete')->middleware('admin');
Route::post('/admin/kategori-produk/aktifasi/{id}', 'KategoriProdukController@aktifasi')->middleware('admin');

# CRUD produk
Route::get('/admin/produk', 'ProdukController@index')->middleware('admin');
Route::get('/admin/produk/detail/{id}', 'ProdukController@detail')->middleware('admin');
Route::get('/admin/data-produk-json', 'ProdukController@getDataJson')->middleware('admin');
Route::get('/admin/produk/form-add', 'ProdukController@formAdd')->middleware('admin');
Route::post('/admin/produk/add-new', 'ProdukController@addNew')->middleware('admin');
Route::get('/admin/produk/form-edit/{id}', 'ProdukController@formEdit')->middleware('admin');
Route::post('/admin/produk/edit/{id}', 'ProdukController@edit')->middleware('admin');
Route::post('/admin/produk/delete/{id}', 'ProdukController@delete')->middleware('admin');
Route::get('/admin/produk/delete-thumbnail/{id}/{thumbnail}', 'ProdukController@deleteThumbnail')->middleware('admin');
Route::get('/admin/produk/delete-foto-pendukung/{idProduk}/{idFoto}/{namaFile}', 'ProdukController@deleteFotoPendukung')->middleware('admin');

# Transaksi
Route::get('/admin/list-transaksi/{id?}', 'AdmTransaksiController@index')->middleware('admin');
Route::get('/admin/get-json-transaksi/{id?}', 'AdmTransaksiController@getTransactionByMonth')->middleware('admin');
Route::get('/admin/transaksi/detail/{invoice}', 'AdmTransaksiController@detail')->middleware('admin');
Route::post('/admin/transaksi/ubah-status/{invoice}', 'AdmTransaksiController@changeStatusTransaction')->middleware('admin');
Route::get('/admin/transaksi/print-invoice/{invoice}', 'AdmTransaksiController@printInvoice')->middleware('admin');
Route::post('/admin/transaksi/input-ongkir-jtr/{invoice}', 'AdmTransaksiController@inputOngkirJtr')->middleware('admin');
Route::get('/admin/transaksi/print-invoice-banyak/{id?}', 'AdmTransaksiController@printInvoiceBanyak')->middleware('admin');
Route::get('/admin/transaksi/get-json-for-print/{id?}', 'AdmTransaksiController@getTransactionForPrint')->middleware('admin');
Route::post('/admin/transaksi/do-print-invoice-banyak', 'AdmTransaksiController@doPrintInvoiceBanyak')->middleware('admin');

# Laporan kategori produk
Route::match(['get', 'post'],'/admin/laporan/laba-kategori-produk', 'AdmLapKategoriProdukController@index')->middleware('admin');
Route::get('/admin/laporan/laba-kategori-produk/detail/{id}/{first}/{last}', 'AdmLapKategoriProdukController@detail')->middleware('admin');

# Laporan produk
Route::match(['get', 'post'],'/admin/laporan/laba-produk', 'AdmLapProdukController@index')->middleware('admin');

# Laporan Distributor
Route::match(['get', 'post'],'/admin/laporan/distributor', 'AdmLapDistributorController@index')->middleware('admin');


///////////////////////////////////////////
//////////////* DISTRIBUTOR *//////////////
///////////////////////////////////////////

# check session & errors
Route::get('/check-session-with-js', 'AuthController@CheckSessionWithJs');
Route::get('/login-ulang', 'AuthController@loginUlang');

# Dashboard
Route::get('/dist/dashboard', 'DistDashboardController@index')->middleware('dist');

# Profile
Route::get('/dist/profile', 'DistProfileController@index')->middleware('dist');
Route::get('/dist/profile/form-edit', 'DistProfileController@formEdit')->middleware('dist');
Route::post('/dist/profile/edit/{id}', 'DistProfileController@edit')->middleware('dist');
Route::get('/dist/form-ubah-password', 'DistProfileController@formUbahPassword')->middleware('dist');
Route::post('/dist/ubah-password', 'DistProfileController@ubahPassword')->middleware('dist');

# Produk
Route::get('/dist/produk/{kategori?}', 'DistProdukController@index')->middleware('dist');
Route::get('/dist/produk/detail/{id}', 'DistProdukController@detail')->middleware('dist');

# Search
Route::match(['get', 'post'],'/dist/produk-search', 'DistProdukController@search')->middleware('dist');

# Keranjang
Route::get('/dist/keranjang', 'DistKeranjangController@index')->middleware('dist');
Route::post('/dist/add-to-cart', 'DistKeranjangController@addToCart')->middleware('dist');
Route::get('/dist/reload-header-count-cart', 'DistKeranjangController@ReloadHeaderCountCart')->middleware('dist');
Route::get('/dist/reload-header-list-cart', 'DistKeranjangController@ReloadHeaderListCart')->middleware('dist');
Route::post('/dist/keranjang/ubah-item', 'DistKeranjangController@updateDataItem')->middleware('dist');
Route::post('/dist/keranjang/hapus-item', 'DistKeranjangController@removeDataItem')->middleware('dist');

# Checkout
Route::get('/dist/checkout', 'DistCheckoutController@index')->middleware('dist');
Route::post('/dist/checkout/buat-pesanan', 'DistCheckoutController@createOrder')->middleware('dist');
Route::get('/dist/pembayaran/{invoice}', 'DistCheckoutController@pembayaran')->middleware('dist');
Route::post('/dist/checkout/get-cost-dropshipper', 'DistCheckoutController@getCostDropshipper')->middleware('dist');
Route::post('/dist/checkout/request-jtr', 'DistCheckoutController@requestJtr')->middleware('dist');
Route::get('/dist/checkout/waiting-request-jtr/{invoice}', 'DistCheckoutController@waitingRequestJtr')->middleware('dist');

# Pesanan
Route::get('/dist/pesanan/{status?}', 'DistPesananController@index')->middleware('dist');
Route::get('/dist/pesanan/detail/{invoice}', 'DistPesananController@detail')->middleware('dist');
Route::post('/dist/pesanan/konfirmasi-sampai', 'DistPesananController@konfirmasiSampai')->middleware('dist');
